<%@ page import="com.nuclearreactor.reactor.NuclearReactorConfiguration" %>



<div class="fieldcontain ${hasErrors(bean: nuclearReactorConfigurationInstance, field: 'getReactorInfoURL', 'error')} ">
	<label for="getReactorInfoURL">
		<g:message code="nuclearReactorConfiguration.getReactorInfoURL.label" default="Get Reactor Info URL" />
		
	</label>
	<g:textField name="getReactorInfoURL" value="${nuclearReactorConfigurationInstance?.getReactorInfoURL}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: nuclearReactorConfigurationInstance, field: 'getTemperatureURL', 'error')} ">
	<label for="getTemperatureURL">
		<g:message code="nuclearReactorConfiguration.getTemperatureURL.label" default="Get Temperature URL" />
		
	</label>
	<g:textField name="getTemperatureURL" value="${nuclearReactorConfigurationInstance?.getTemperatureURL}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: nuclearReactorConfigurationInstance, field: 'setValveStateURL', 'error')} ">
	<label for="setValveStateURL">
		<g:message code="nuclearReactorConfiguration.setValveStateURL.label" default="Set Valve State URL" />
		
	</label>
	<g:textField name="setValveStateURL" value="${nuclearReactorConfigurationInstance?.setValveStateURL}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: nuclearReactorConfigurationInstance, field: 'setRodsStateURL', 'error')} ">
	<label for="setRodsStateURL">
		<g:message code="nuclearReactorConfiguration.setRodsStateURL.label" default="Set Rods State URL" />
		
	</label>
	<g:textField name="setRodsStateURL" value="${nuclearReactorConfigurationInstance?.setRodsStateURL}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: nuclearReactorConfigurationInstance, field: 'setPumpStateURL', 'error')} ">
	<label for="setPumpStateURL">
		<g:message code="nuclearReactorConfiguration.setPumpStateURL.label" default="Set Pump State URL" />
		
	</label>
	<g:textField name="setPumpStateURL" value="${nuclearReactorConfigurationInstance?.setPumpStateURL}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: nuclearReactorConfigurationInstance, field: 'setHeaterStateURL', 'error')} ">
	<label for="setHeaterStateURL">
		<g:message code="nuclearReactorConfiguration.setHeaterStateURL.label" default="Set Heater State URL" />
		
	</label>
	<g:textField name="setHeaterStateURL" value="${nuclearReactorConfigurationInstance?.setHeaterStateURL}"/>

</div>


	<input type="hidden" name="nuclearReactor.id" value="${nuclearReactorConfigurationInstance?.nuclearReactor?.id}"/></div>

