
<%@ page import="com.nuclearreactor.reactor.NuclearReactorConfiguration" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'nuclearReactorConfiguration.label', default: 'NuclearReactorConfiguration')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-nuclearReactorConfiguration" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-nuclearReactorConfiguration" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="getReactorInfoURL" title="${message(code: 'nuclearReactorConfiguration.getReactorInfoURL.label', default: 'Get Reactor Info URL')}" />
					
						<g:sortableColumn property="getTemperatureURL" title="${message(code: 'nuclearReactorConfiguration.getTemperatureURL.label', default: 'Get Temperature URL')}" />
					
						<g:sortableColumn property="setValveStateURL" title="${message(code: 'nuclearReactorConfiguration.setValveStateURL.label', default: 'Set Valve State URL')}" />
					
						<g:sortableColumn property="setRodsStateURL" title="${message(code: 'nuclearReactorConfiguration.setRodsStateURL.label', default: 'Set Rods State URL')}" />
					
						<g:sortableColumn property="setPumpStateURL" title="${message(code: 'nuclearReactorConfiguration.setPumpStateURL.label', default: 'Set Pump State URL')}" />
					
						<g:sortableColumn property="setHeaterStateURL" title="${message(code: 'nuclearReactorConfiguration.setHeaterStateURL.label', default: 'Set Heater State URL')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${nuclearReactorConfigurationInstanceList}" status="i" var="nuclearReactorConfigurationInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${nuclearReactorConfigurationInstance.id}">${fieldValue(bean: nuclearReactorConfigurationInstance, field: "getReactorInfoURL")}</g:link></td>
					
						<td>${fieldValue(bean: nuclearReactorConfigurationInstance, field: "getTemperatureURL")}</td>
					
						<td>${fieldValue(bean: nuclearReactorConfigurationInstance, field: "setValveStateURL")}</td>
					
						<td>${fieldValue(bean: nuclearReactorConfigurationInstance, field: "setRodsStateURL")}</td>
					
						<td>${fieldValue(bean: nuclearReactorConfigurationInstance, field: "setPumpStateURL")}</td>
					
						<td>${fieldValue(bean: nuclearReactorConfigurationInstance, field: "setHeaterStateURL")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${nuclearReactorConfigurationInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
