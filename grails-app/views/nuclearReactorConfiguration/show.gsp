
<%@ page import="com.nuclearreactor.reactor.NuclearReactorConfiguration" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'nuclearReactorConfiguration.label', default: 'NuclearReactorConfiguration')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-nuclearReactorConfiguration" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">

		</div>
		<div id="show-nuclearReactorConfiguration" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list nuclearReactorConfiguration">
			
				<g:if test="${nuclearReactorConfigurationInstance?.getReactorInfoURL}">
				<li class="fieldcontain">
					<span id="getReactorInfoURL-label" class="property-label"><g:message code="nuclearReactorConfiguration.getReactorInfoURL.label" default="Get Reactor Info URL" /></span>
					
						<span class="property-value" aria-labelledby="getReactorInfoURL-label"><g:fieldValue bean="${nuclearReactorConfigurationInstance}" field="getReactorInfoURL"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${nuclearReactorConfigurationInstance?.getTemperatureURL}">
				<li class="fieldcontain">
					<span id="getTemperatureURL-label" class="property-label"><g:message code="nuclearReactorConfiguration.getTemperatureURL.label" default="Get Temperature URL" /></span>
					
						<span class="property-value" aria-labelledby="getTemperatureURL-label"><g:fieldValue bean="${nuclearReactorConfigurationInstance}" field="getTemperatureURL"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${nuclearReactorConfigurationInstance?.setValveStateURL}">
				<li class="fieldcontain">
					<span id="setValveStateURL-label" class="property-label"><g:message code="nuclearReactorConfiguration.setValveStateURL.label" default="Set Valve State URL" /></span>
					
						<span class="property-value" aria-labelledby="setValveStateURL-label"><g:fieldValue bean="${nuclearReactorConfigurationInstance}" field="setValveStateURL"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${nuclearReactorConfigurationInstance?.setRodsStateURL}">
				<li class="fieldcontain">
					<span id="setRodsStateURL-label" class="property-label"><g:message code="nuclearReactorConfiguration.setRodsStateURL.label" default="Set Rods State URL" /></span>
					
						<span class="property-value" aria-labelledby="setRodsStateURL-label"><g:fieldValue bean="${nuclearReactorConfigurationInstance}" field="setRodsStateURL"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${nuclearReactorConfigurationInstance?.setPumpStateURL}">
				<li class="fieldcontain">
					<span id="setPumpStateURL-label" class="property-label"><g:message code="nuclearReactorConfiguration.setPumpStateURL.label" default="Set Pump State URL" /></span>
					
						<span class="property-value" aria-labelledby="setPumpStateURL-label"><g:fieldValue bean="${nuclearReactorConfigurationInstance}" field="setPumpStateURL"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${nuclearReactorConfigurationInstance?.setHeaterStateURL}">
				<li class="fieldcontain">
					<span id="setHeaterStateURL-label" class="property-label"><g:message code="nuclearReactorConfiguration.setHeaterStateURL.label" default="Set Heater State URL" /></span>
					
						<span class="property-value" aria-labelledby="setHeaterStateURL-label"><g:fieldValue bean="${nuclearReactorConfigurationInstance}" field="setHeaterStateURL"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${nuclearReactorConfigurationInstance?.nuclearReactor}">
				<li class="fieldcontain">
					<span id="nuclearReactor-label" class="property-label"><g:message code="nuclearReactorConfiguration.nuclearReactor.label" default="Nuclear Reactor" /></span>
					
						<span class="property-value" aria-labelledby="nuclearReactor-label"><a href="javascript:void(0)">${nuclearReactorConfigurationInstance?.nuclearReactor as grails.converters.JSON}</a></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:nuclearReactorConfigurationInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${nuclearReactorConfigurationInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
