
<%@ page import="com.nuclearreactor.reactor.PumpState; com.nuclearreactor.reactor.NuclearReactor" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'nuclearReactor.label', default: 'NuclearReactor')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-nuclearReactor" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
		</div>
		<div id="show-nuclearReactor" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list nuclearReactor">
			

				<g:if test="${nuclearReactorInstance?.code}">
				<li class="fieldcontain">
					<span id="code-label" class="property-label"><g:message code="nuclearReactor.code.label" default="Code" /></span>
					
						<span class="property-value" aria-labelledby="code-label"><g:fieldValue bean="${nuclearReactorInstance}" field="code"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${nuclearReactorInstance?.lastUpdate}">
				<li class="fieldcontain">
					<span id="lastUpdate-label" class="property-label"><g:message code="nuclearReactor.lastUpdate.label" default="Last Update" /></span>
					
						<span class="property-value" aria-labelledby="lastUpdate-label"><g:formatDate date="${nuclearReactorInstance?.lastUpdate}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${nuclearReactorInstance?.pumpState}">
				<li class="fieldcontain">
					<span id="pumpState-label" class="property-label"><g:message code="nuclearReactor.pumpState.label" default="Pump State" /></span>
					
						<span class="property-value" aria-labelledby="pumpState-label">
							<g:fieldValue bean="${nuclearReactorInstance}" field="pumpState"/>
							<g:form controller="nuclearReactorRest" action="setPumpState" method="post" >
								<input type="hidden" name="user" value="hola"/>
								<input type="hidden" name="validationKey" value="d9253ab3d21fc258e32764e56f825a12"/>
								<input type="hidden" name="reactorCode" value="${nuclearReactorInstance.code}"/>
								<input type="hidden" name="pumpState" value="${(nuclearReactorInstance.pumpState.state == 'ON '? 'OFF' : 'ON' )}"/>
								<input type="submit" value="Change it!"/>
							</g:form>
						</span>
					</span>
					
				</li>
				</g:if>
			
				<g:if test="${nuclearReactorInstance?.rodsState}">
				<li class="fieldcontain">
					<span id="rodsState-label" class="property-label"><g:message code="nuclearReactor.rodsState.label" default="Rods State" /></span>
					
						<span class="property-value" aria-labelledby="rodsState-label">
							<g:fieldValue bean="${nuclearReactorInstance}" field="rodsState"/>
							<g:form controller="nuclearReactorRest" action="setRodsState" method="post" >
								<input type="hidden" name="user" value="hola"/>
								<input type="hidden" name="validationKey" value="d9253ab3d21fc258e32764e56f825a12"/>
								<input type="hidden" name="reactorCode" value="${nuclearReactorInstance.code}"/>
								<input type="hidden" name="rodsState" value="${(nuclearReactorInstance.rodsState.state == 'Arriba ' ? 'OFF' : 'ON' )}"/>
								<input type="submit" value="Change it!"/>
							</g:form>
						</span>
					
				</li>
				</g:if>
			
				<g:if test="${nuclearReactorInstance?.heaterState}">
				<li class="fieldcontain">
					<span id="heaterState-label" class="property-label"><g:message code="nuclearReactor.heaterState.label" default="Heater State" /></span>
					
						<span class="property-value" aria-labelledby="heaterState-label">
							<g:fieldValue bean="${nuclearReactorInstance}" field="heaterState"/>
							<g:form controller="nuclearReactorRest" action="setHeaterState" method="post" >
								<input type="hidden" name="user" value="hola"/>
								<input type="hidden" name="validationKey" value="d9253ab3d21fc258e32764e56f825a12"/>
								<input type="hidden" name="reactorCode" value="${nuclearReactorInstance.code}"/>
								<input type="hidden" name="heaterState" value="${(nuclearReactorInstance.heaterState.state == 'ON ' ? 'OFF' : 'ON' )}"/>
								<input type="submit" value="Change it!"/>
							</g:form>
						</span>
					
				</li>
				</g:if>
			
				<g:if test="${nuclearReactorInstance?.configuration}">
				<li class="fieldcontain">
					<span id="configuration-label" class="property-label"><g:message code="nuclearReactor.configuration.label" default="Configuration" /></span>
					
						<span class="property-value" aria-labelledby="configuration-label"><g:link controller="nuclearReactorConfiguration" action="show" id="${nuclearReactorInstance?.configuration?.id}">Ver Configuración</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${nuclearReactorInstance?.valves}">
				<li class="fieldcontain">
					<span id="valves-label" class="property-label"><g:message code="nuclearReactor.valves.label" default="Valves" /></span>
					
						<g:each in="${nuclearReactorInstance.valves}" var="v">
							<span class="property-value" aria-labelledby="valves-label">
								<span>${v as grails.converters.JSON}</span>
								<g:form controller="nuclearReactorRest" action="setValveState" method="post" >
									<input type="hidden" name="user" value="hola"/>
									<input type="hidden" name="validationKey" value="d9253ab3d21fc258e32764e56f825a12"/>
									<input type="hidden" name="reactorCode" value="${nuclearReactorInstance.code}"/>
									<input type="hidden" name="valveIdentification" value="${v.identifier}"/>
									<input type="hidden" name="valveState" value="${(v.state.state == 'ON ' ? 'OFF' : 'ON' )}"/>
									<input type="submit" value="Change it!"/>
								</g:form>
							</span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${nuclearReactorInstance?.coolantSystem}">
				<li class="fieldcontain">
					<span id="coolantSystem-label" class="property-label"><g:message code="nuclearReactor.coolantSystem.label" default="Coolant System" /></span>
					
						<span class="property-value" aria-labelledby="coolantSystem-label"><a href="javascript:void(0)">${nuclearReactorInstance?.coolantSystem as grails.converters.JSON}</a></span>
					
				</li>
				</g:if>
			
			</ol>
		</div>
	</body>
</html>
