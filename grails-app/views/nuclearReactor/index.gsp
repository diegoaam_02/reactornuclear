
<%@ page import="com.nuclearreactor.reactor.NuclearReactor" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'nuclearReactor.label', default: 'NuclearReactor')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-nuclearReactor" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-nuclearReactor" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<th><g:message code="nuclearReactor.company.label" default="Company" /></th>
					
						<g:sortableColumn property="code" title="${message(code: 'nuclearReactor.code.label', default: 'Code')}" />
					
						<g:sortableColumn property="lastUpdate" title="${message(code: 'nuclearReactor.lastUpdate.label', default: 'Last Update')}" />
					
						<g:sortableColumn property="pumpState" title="${message(code: 'nuclearReactor.pumpState.label', default: 'Pump State')}" />
					
						<g:sortableColumn property="rodsState" title="${message(code: 'nuclearReactor.rodsState.label', default: 'Rods State')}" />
					
						<g:sortableColumn property="heaterState" title="${message(code: 'nuclearReactor.heaterState.label', default: 'Heater State')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${nuclearReactorInstanceList}" status="i" var="nuclearReactorInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${nuclearReactorInstance.id}">${fieldValue(bean: nuclearReactorInstance, field: "company")}</g:link></td>
					
						<td>${fieldValue(bean: nuclearReactorInstance, field: "code")}</td>
					
						<td><g:formatDate date="${nuclearReactorInstance.lastUpdate}" /></td>
					
						<td>${fieldValue(bean: nuclearReactorInstance, field: "pumpState")}</td>
					
						<td>${fieldValue(bean: nuclearReactorInstance, field: "rodsState")}</td>
					
						<td>${fieldValue(bean: nuclearReactorInstance, field: "heaterState")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${nuclearReactorInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
