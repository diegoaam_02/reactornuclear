<%@ page import="com.nuclearreactor.reactor.NuclearReactor" %>



<div class="fieldcontain ${hasErrors(bean: nuclearReactorInstance, field: 'company', 'error')} required">
	<label for="company">
		<g:message code="nuclearReactor.company.label" default="Company" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="company" name="company.id" from="${com.nuclearreactor.accounts.CompanyAccount.list()}" optionKey="id" required="" value="${nuclearReactorInstance?.company?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: nuclearReactorInstance, field: 'code', 'error')} required">
	<label for="code">
		<g:message code="nuclearReactor.code.label" default="Code" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="code" required="" value="${nuclearReactorInstance?.code}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: nuclearReactorInstance, field: 'lastUpdate', 'error')} ">
	<label for="lastUpdate">
		<g:message code="nuclearReactor.lastUpdate.label" default="Last Update" />
		
	</label>
	<g:datePicker name="lastUpdate" precision="day"  value="${nuclearReactorInstance?.lastUpdate}" default="none" noSelection="['': '']" />

</div>

<div class="fieldcontain ${hasErrors(bean: nuclearReactorInstance, field: 'pumpState', 'error')} ">
	<label for="pumpState">
		<g:message code="nuclearReactor.pumpState.label" default="Pump State" />
		
	</label>
	<g:select name="pumpState" from="${com.nuclearreactor.reactor.PumpState?.values()}" keys="${com.nuclearreactor.reactor.PumpState.values()*.name()}" value="${nuclearReactorInstance?.pumpState?.name()}"  noSelection="['': '']"/>

</div>

<div class="fieldcontain ${hasErrors(bean: nuclearReactorInstance, field: 'rodsState', 'error')} ">
	<label for="rodsState">
		<g:message code="nuclearReactor.rodsState.label" default="Rods State" />
		
	</label>
	<g:select name="rodsState" from="${com.nuclearreactor.reactor.RodsState?.values()}" keys="${com.nuclearreactor.reactor.RodsState.values()*.name()}" value="${nuclearReactorInstance?.rodsState?.name()}"  noSelection="['': '']"/>

</div>

<div class="fieldcontain ${hasErrors(bean: nuclearReactorInstance, field: 'heaterState', 'error')} ">
	<label for="heaterState">
		<g:message code="nuclearReactor.heaterState.label" default="Heater State" />
		
	</label>
	<g:select name="heaterState" from="${com.nuclearreactor.reactor.HeaterState?.values()}" keys="${com.nuclearreactor.reactor.HeaterState.values()*.name()}" value="${nuclearReactorInstance?.heaterState?.name()}"  noSelection="['': '']"/>

</div>

<div class="fieldcontain ${hasErrors(bean: nuclearReactorInstance, field: 'configuration', 'error')} ">
	<label for="configuration">
		<g:message code="nuclearReactor.configuration.label" default="Configuration" />
		
	</label>
	<g:select id="configuration" name="configuration.id" from="${com.nuclearreactor.reactor.NuclearReactorConfiguration.list()}" optionKey="id" value="${nuclearReactorInstance?.configuration?.id}" class="many-to-one" noSelection="['null': '']"/>

</div>

<div class="fieldcontain ${hasErrors(bean: nuclearReactorInstance, field: 'valves', 'error')} ">
	<label for="valves">
		<g:message code="nuclearReactor.valves.label" default="Valves" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${nuclearReactorInstance?.valves?}" var="v">
    <li><g:link controller="valve" action="show" id="${v.id}">${v?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="valve" action="create" params="['nuclearReactor.id': nuclearReactorInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'valve.label', default: 'Valve')])}</g:link>
</li>
</ul>


</div>

<div class="fieldcontain ${hasErrors(bean: nuclearReactorInstance, field: 'alertsConfiguration', 'error')} ">
	<label for="alertsConfiguration">
		<g:message code="nuclearReactor.alertsConfiguration.label" default="Alerts Configuration" />
		
	</label>
	<g:select id="alertsConfiguration" name="alertsConfiguration.id" from="${com.nuclearreactor.alerts.AlertsConfiguration.list()}" optionKey="id" value="${nuclearReactorInstance?.alertsConfiguration?.id}" class="many-to-one" noSelection="['null': '']"/>

</div>

<div class="fieldcontain ${hasErrors(bean: nuclearReactorInstance, field: 'coolantSystem', 'error')} required">
	<label for="coolantSystem">
		<g:message code="nuclearReactor.coolantSystem.label" default="Coolant System" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="coolantSystem" name="coolantSystem.id" from="${com.nuclearreactor.reactor.CoolantSystem.list()}" optionKey="id" required="" value="${nuclearReactorInstance?.coolantSystem?.id}" class="many-to-one"/>

</div>

