package com.nuclearreactor.jobs.reactor

/**
 * Created by hernan on 23/11/15.
 */
class DataBaseJob {

    def group = "DataBaseJob"
    def description = "This job is executed each minute"

    def databaseService

    static triggers = {
        simple name: 'normalTrigger', startDelay: 5000, repeatInterval: 5000
    }

    def execute() {
        log.info "Executing Cron Job to update a local database from remote"

        if(databaseService.checkStoredRequest()){
            databaseService.pullRemoteToLocal();
        }

    }

}
