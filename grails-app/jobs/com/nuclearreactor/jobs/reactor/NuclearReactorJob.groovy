package com.nuclearreactor.jobs.reactor
/**
 * This class executes a job
 */
class NuclearReactorJob {

    def group = "NuclearReactorJob"
    def description = "This job is executed each minute"

    def nuclearReactorService
    def databaseService

    /**
     * Define is the service is running local or online
     */
    def isLocal = false

    static triggers = {
       simple name: 'normalTrigger', startDelay: 5000, repeatInterval: 142000
    }

    def execute() {
        log.info "Executing Cron Job to get current state and send alerts"

        if (isLocal){
            if (databaseService.checkStoredRequest()) {
                databaseService.pullRemoteToLocal();
            }
        }

        nuclearReactorService.updateAllNuclearReactorsFromRemote(true)
    }
}
