package com.nuclearreactor.audits

import com.nuclearreactor.reactor.NuclearReactor

class Audits {

    String user
    OperationType operation
    Date date

    static belongsTo = [
            nuclearReactor : NuclearReactor
    ]

    static constraints = {
        user nullable: true
        operation nullable: false
        date nullable: false
        nuclearReactor nullable: false
    }


}
enum OperationType{
    SET_RODS_STATE,
    SET_VALVE_STATE,
    SET_HEATER_STATE,
    GET_REACTOR_INFO,
    GET_TEMP_INFO
}
