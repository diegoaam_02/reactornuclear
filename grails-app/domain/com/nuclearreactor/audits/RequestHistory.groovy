package com.nuclearreactor.audits

/**
 * Created by hernan on 23/11/15.
 */
class RequestHistory {

    String request
    Date requestDate
    String requestType

    static constraints = {

        request nullable: false, blank: false
        requestDate nullable: false, blank: false
        requestType nullable: false, blank: false

    }


}
