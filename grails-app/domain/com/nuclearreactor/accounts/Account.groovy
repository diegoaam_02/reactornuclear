package com.nuclearreactor.accounts

import com.nuclearreactor.users.User

/**
 * Representation of an Account in weservice
 *
 * @author <a href="emailto:diegoaam_02@hotmail.com">Diego Amaya</a>
 * @version 27/11/2014 - <a href="emailto:diegoaam_02@hotmail.com">Diego Amaya</a> - First Version
 * @since 27/11/2014 Copyright © All Rights Reserved.
 */
class Account {

    String firstName
    String lastName
    String preferredLocale
    String identificationNumber
    String address
    String phoneNumber

    byte[] avatarImage

    AccountType accountType

    User user

    static constraints = {
        firstName nullable: false, blank: false
        lastName blank: false, nullable: true
        preferredLocale blank: false, nullable: false
        identificationNumber nullable: true, blank: false
        address nullable: true
        phoneNumber nullable: true
        user nullable: true
        avatarImage nullable: true
        accountType nullable: false
    }

    static mapping = {
        tablePerHierarchy false
    }


}

public enum AccountType {
    Principal("Pri"),
    Customer("Cus");

    final String id

    AccountType(String id) {
        this.id = id
    }

    String toString() {
        id
    }

    String getKey() {
        name()
    }
}
