package com.nuclearreactor.accounts

/**
 * Representation of a Company account in the system
 *
 * @author <a href="emailto:diegoaam_02@hotmail.com">Diego Amaya</a>
 * @version 28/11/2014 - <a href="emailto:diegoaam_02@hotmail.com">Diego Amaya</a> - First Version
 * @since 28/11/2014 Copyright © All Rights Reserved.
 */
class CompanyAccount extends Account{

    String description

    static constraints = {
        description nullable: true
    }
}
