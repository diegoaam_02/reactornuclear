package com.nuclearreactor.users

import com.nuclearreactor.accounts.Account

/**
 * Representation of an User
 *
 * @author <a href="emailto:diegoaam_02@hotmail.com">Diego Amaya</a>
 * @version 17/08/2015 - <a href="emailto:diegoaam_02@hotmail.com">Diego Amaya</a> - First Version
 * @since 17/08/2015 Copyright � All Rights Reserved.
 */
class User implements Serializable {

	private static final long serialVersionUID = 1

	transient springSecurityService

	String email
	String password
	boolean enabled = true
	String authenticationToken
	boolean accountExpired
	boolean accountLocked
	boolean passwordExpired
	Date sinceDate = new Date()

	User(String username, String password) {
		//this()
		this.email = username
		this.password = password
	}

	static belongsTo = [
			account:Account
	]

	@Override
	int hashCode() {
		email?.hashCode() ?: 0
	}

	@Override
	boolean equals(other) {
		is(other) || (other instanceof User && other.email == email)
	}

	@Override
	String toString() {
		email
	}

	Set<Role> getAuthorities() {
		UserRole.findAllByUser(this)*.role
	}

	def beforeInsert() {
		encodePassword()
	}

	def beforeUpdate() {
		if (isDirty('password')) {
			encodePassword()
		}
	}

	protected void encodePassword() {
		password = springSecurityService?.passwordEncoder ? springSecurityService.encodePassword(password) : password
	}

	static transients = ['springSecurityService']

	static constraints = {
		email blank: false, unique: true, matches: "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})\$"
		password blank: false, nullable: false, minSize: 5
		authenticationToken blank: false, nullable: true, unique: true
		account nullable: true
		sinceDate nullable: false
	}

	static mapping = {
		password column: '`password`'
		table name: 'nr_user'
	}
}
