package com.nuclearreactor.alerts

/**
 * Created by diegoamaya on 3/09/15.
 */
enum AlertType {
    Information("I"),
    Warning("W"),
    Danger("D")

    private final String type;

    public AlertType(String type){
        this.type = type
    }

    public static AlertType fromStringToEnum(String type){
        for(AlertType value: values()){
            if(type.equals(value.type)){
                return value
            }
        }
    }

    public String getState(){
        return state;
    }
}