package com.nuclearreactor.alerts

import com.nuclearreactor.reactor.NuclearReactor

/**
 * Alert Entity
 */
class Alert {

    AlertType type
    String message
    Date alertDate = new Date()

    static belongsTo = [
            nuclearReactor : NuclearReactor
    ]
    static constraints = {
        type nullable: false
        message nullable: false
        nuclearReactor nullable: false
        alertDate nullable: false
    }
}
