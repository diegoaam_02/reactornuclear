package com.nuclearreactor.alerts

import com.nuclearreactor.reactor.NuclearReactor

class AlertsConfiguration {

    //TEMPERATURE CONFIGURATION
    Float infoMaxTempPercentage
    Float warningMaxTempPercentage
    Float dangerMaxTempPercentage

    //CHANNEL CONFIGURATION
    Boolean sendWebSocketAlert = true
    Boolean sendEmailAlert = true
    Boolean sendRestAlert = true

    //USER CONFIGURATION
    Boolean sendAlertToOperator
    Boolean sendAlertToAdministrator

    static belongsTo = [
            nuclearReactor : NuclearReactor
    ]

    static constraints = {
        infoMaxTempPercentage nullable: false, min: 0.0F
        warningMaxTempPercentage nullable: false, min: 0.0F
        dangerMaxTempPercentage nullable: false, min: 0.0F
        nuclearReactor nullable: false
        sendWebSocketAlert nullable: false
        sendEmailAlert nullable: false
        sendAlertToOperator nullable: false
        sendAlertToAdministrator nullable: false
        sendRestAlert nullable: false
    }
}
