package com.nuclearreactor.reactor

class NuclearReactorConfiguration {

    String getReactorInfoURL
    String getTemperatureURL
    String setValveStateURL
    String setRodsStateURL
    String setPumpStateURL
    String setHeaterStateURL



    static belongsTo = [
            nuclearReactor : NuclearReactor
    ]
    static constraints = {
        getReactorInfoURL nullable: true, blank: false
        getTemperatureURL nullable: true, blank: false
        setValveStateURL nullable: true, blank: false
        setRodsStateURL nullable: true, blank: false
        setPumpStateURL nullable: true, blank: false
        setHeaterStateURL nullable: true, blank: false
        nuclearReactor nullable: false
    }

}
