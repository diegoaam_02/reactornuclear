package com.nuclearreactor.reactor

/**
 * Representation of a Coolant System
 *
 * @author <a href="emailto:diegoaam_02@hotmail.com">Diego Amaya</a>
 * @version 17/08/2015 - <a href="emailto:diegoaam_02@hotmail.com">Diego Amaya</a> - First Version
 * @since 17/08/2015.
 */
class CoolantSystem {

    Float humidityPercentage

    Double currentTemperature
    Double idealTemperature

    //Galones/Minuto
    Double waterPressure

    static belongsTo = [nuclearReactor : NuclearReactor]


    static constraints = {
        humidityPercentage nullable: false, min: 0.0F, max: 1.0F
        currentTemperature nullable: false
        waterPressure nullable: true
        idealTemperature nullable: false
    }
}
