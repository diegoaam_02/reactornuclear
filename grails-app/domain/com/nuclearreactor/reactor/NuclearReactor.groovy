package com.nuclearreactor.reactor

import com.nuclearreactor.accounts.CompanyAccount
import com.nuclearreactor.alerts.AlertsConfiguration

/**
 * Representation of a Nuclear Reactor
 *
 * @author <a href="emailto:diegoaam_02@hotmail.com">Diego Amaya</a>
 * @version 17/08/2015 - <a href="emailto:diegoaam_02@hotmail.com">Diego Amaya</a> - First Version
 * @since 17/08/2015
 */
class NuclearReactor {

    String code

    //Bomba
    PumpState pumpState
    //Barras
    RodsState rodsState
    //Resistencia/Calentador
    HeaterState heaterState

    Date lastUpdate


    static hasOne = [
            coolantSystem: CoolantSystem,
            configuration: NuclearReactorConfiguration,
            alertsConfiguration: AlertsConfiguration

    ]

    static hasMany = [
            valves : Valve
    ]

    static belongsTo = [
            company: CompanyAccount
    ]

    static fetchMode = [
            configuration: 'eager',
            coolantSystem: 'eager',
            alertsConfiguration: 'lazy'
    ]

    static constraints = {
        company nullable: false
        code nullable: false, blank: false
        lastUpdate nullable: true
        pumpState nullable: true
        rodsState nullable: true
        heaterState nullable: true
        configuration nullable: true
        valves nullable: true
        alertsConfiguration nullable: true
    }

    @Override
    public String toString() {
        return "NuclearReactor{" +
                "id=" + id +
                ", code='" + code + '\'' +
                ", pumpState=" + pumpState +
                ", rodsState=" + rodsState +
                ", heaterState=" + heaterState +
                ", lastUpdate=" + lastUpdate +
                ", version=" + version +
                ", valves=" + valves +
                ", alertsConfiguration=" + alertsConfiguration +
                ", coolantSystem=" + coolantSystem +
                ", configuration=" + configuration +
                ", company=" + company +
                '}';
    }
}
enum RodsState{
    Up("Arriba"),
    Down("Abajo")

    private final String state;

    public RodsState(String state){
        this.state = state
    }

    public static RodsState fromStringToEnum(String state){
        for(RodsState value: values()){
            if(state.equals(value.state)){
                return value
            }
        }
    }

    public String getState(){
        return state
    }
}
enum PumpState {
    Active("ON"),
    Inactive("OFF")

    private final String state;

    public PumpState(String state) {
        this.state = state
    }

    public static PumpState fromStringToEnum(String state) {
        for (PumpState value : values()) {
            if (state.equals(value.state)) {
                return value
            }
        }
    }
    public String getState(){
        return state;
    }
}

enum HeaterState{
    Active("ON"),
    Inactive("OFF")

    private final String state;

    public HeaterState(String state){
        this.state = state
    }

    public static HeaterState fromStringToEnum(String state){
        for(HeaterState value: values()){
            if(state.equals(value.state)){
                return value
            }
        }
    }

public String getState(){
    return state;
}
}