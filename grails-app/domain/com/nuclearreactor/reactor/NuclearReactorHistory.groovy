package com.nuclearreactor.reactor

/**
 * Created by diegoamaya on 29/08/15.
 */
class NuclearReactorHistory {

    String code

    Date lastUpdate

    //Bomba
    PumpState pumpState
    //Barras
    RodsState rodsState
    //Resistencia/Calentador
    HeaterState heaterState

    Date date

    Float humidityPercentage

    Double currentTemperature
    Double idealTemperature

    String valvesState

    static constraints = {
        valvesState nullable: false, blank: false
        idealTemperature nullable: false
        currentTemperature nullable: false
        humidityPercentage nullable: true
        rodsState nullable: false
        pumpState nullable: false
        heaterState nullable: true
        lastUpdate nullable: false
        date nullable: false
        code nullable: false
    }
    static belongsTo = [
            nuclearReactor : NuclearReactor
    ]

    static fetchMode = [
            nuclearReactor : 'lazy'
    ]
}
