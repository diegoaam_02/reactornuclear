package com.nuclearreactor.reactor

/**
 * Representation of Valve that belongs to the reactor
 *
 * @author <a href="emailto:diegoaam_02@hotmail.com">Diego Amaya</a>
 * @version 17/08/2015 - <a href="emailto:diegoaam_02@hotmail.com">Diego Amaya</a> - First Version
 * @since 17/08/2015.
 */
class Valve {

    ValveState state
    String identifier

    static belongsTo = [
            nuclearReactor : NuclearReactor
    ]

    static constraints = {
        state nullable: false
        identifier nullable: false, unique: true, blank: false
        nuclearReactor nullable: false
    }
}
enum ValveState{
    Open("ON"),
    Closed("OFF")

    private final String state

    public ValveState(String state){
        this.state = state
    }

    public static ValveState fromStringToEnum(String state){
        for(ValveState value: values()){
            if(state.equals(value.state)){
                return value
            }
        }
    }

    public String getState(){
        return state
    }
}