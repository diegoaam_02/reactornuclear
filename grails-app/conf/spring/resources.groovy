import com.nuclearreactor.alerts.impl.AlertServiceImpl
import com.nuclearreactor.reactor.impl.DatabaseServiceImpl
import com.nuclearreactor.reactor.impl.NuclearReactorHistoryServiceImpl
import com.nuclearreactor.util.alerts.AlertStrategyFactory
import com.nuclearreactor.util.alerts.impl.AlertRestStrategyServiceImpl
import com.nuclearreactor.util.alerts.impl.AlertStrategyManagerImpl
import com.nuclearreactor.util.alerts.impl.AlertWebSocketStrategyServiceImpl
import com.nuclearreactor.util.alerts.impl.AlertEmailStrategyServiceImpl
import com.nuclearreactor.reactor.impl.NuclearReactorServiceImpl
import com.nuclearreactor.reactor.impl.ValveServiceImpl
import com.nuclearreactor.reactor.restClient.impl.ModelMonitorClientServiceImpl
import grails.plugin.mail.MailService
import grails.plugins.rest.client.RestBuilder
import org.springframework.beans.factory.config.ServiceLocatorFactoryBean

// Place your Spring DSL code here
beans = {

    /**
     * ******************************************
     *             REACTOR COMPONENTS
     * ******************************************
     */

    /**
     * Nuclear Reactor Bean
     */
    nuclearReactorService(NuclearReactorServiceImpl) {
        modelMonitorClientService = ref('modelMonitorClientService')
        alertService = ref('alertService')
        nuclearReactorHistoryService = ref('nuclearReactorHistoryService')
    }

    /**
     * Valve Service
     */
    valveService(ValveServiceImpl){
        modelMonitorClientService = ref('modelMonitorClientService')
    }

    /**
     * ******************************************
     *                REST CLIENT
     * ******************************************
     */
    /**
     * Model Monitor Client Bean
     * REST CLIENT TO MODEL
     */
    modelMonitorClientService(ModelMonitorClientServiceImpl){
        restBuilder = ref('restBuilder')
    }

    /**
     * *******************************************
     *                  ALERTS
     * *******************************************
     */
    /**
     * Factory for alert strategy services
     */
    alertStrategyFactory(ServiceLocatorFactoryBean){
        serviceLocatorInterface = AlertStrategyFactory.class
    }
    /**
     * Alert Strategy Manager
     */
    alertStrategyManager(AlertStrategyManagerImpl){
        alertStrategyFactory = ref('alertStrategyFactory')
    }

    /**
     * Alert Web Socket Strategy Service
     */
    alertWebSocketStrategyService(AlertWebSocketStrategyServiceImpl){
        brokerMessagingTemplate = ref('brokerMessagingTemplate')
    }

    alertRestStrategyService(AlertRestStrategyServiceImpl){
        restBuilder = ref('restBuilder')
    }

    alertEmailStrategyService(AlertEmailStrategyServiceImpl){
        mailService = ref('mailService')
    }

    alertService(AlertServiceImpl){
        alertStrategyManager = ref('alertStrategyManager')
    }

    restBuilder(RestBuilder){}

    nuclearReactorHistoryService(NuclearReactorHistoryServiceImpl)

    databaseService(DatabaseServiceImpl)

    /**
     * Aspects
     */


    xmlns aop:"http://www.springframework.org/schema/aop"
    aspectBean(com.nuclearreactor.audits.AuditAspect)
    aop.config("proxy-target-class":true) {}



}
