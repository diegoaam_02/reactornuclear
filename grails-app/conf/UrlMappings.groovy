class UrlMappings {

	static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/"(controller: 'nuclearReactor', action: 'index')
        "500"(view:'/error')

        /**
         * REST
         */

        //REST SERVICE TO UPDATE THE VALVE STATE TO THE MODEL, THE ARGUMENTS CAN ALSO BE SEND AS HTTP PARAMS OR BY THE LINK BELOW
        "/nuclearReactorRest/setValveState/$reactorCode/$valveIdentification/$valveState"(controller: 'nuclearReactorRest', action: 'setValveState')

        //REST SERVICE TO UPDATE THE RODS STATE TO THE MODEL, THE ARGUMENTS CAN ALSO BE SEND AS HTTP PARAMS OR BY THE LINK BELOW
        "/nuclearReactorRest/setRodsState/$reactorCode/$rodsState"(controller: 'nuclearReactorRest', action: 'setRodsState')

        //REST SERVICE TO UPDATE THE PUMP STATE TO THE MODEL, THE ARGUMENTS CAN ALSO BE SEND AS HTTP PARAMS OR BY THE LINK BELOW
        "/nuclearReactorRest/setPumpState/$reactorCode/$pumpState"(controller: 'nuclearReactorRest', action: 'setPumpState')
        //REST SERVICE TO UPDATE THE HEATER STATE TO THE MODEL, THE ARGUMENTS CAN ALSO BE SEND AS HTTP PARAMS OR BY THE LINK BELOW
        "/nuclearReactorRest/setHeaterState/$reactorCode/$heaterState"(controller: 'nuclearReactorRest', action: 'setHeaterState')

        //TEST
        "/nuclearReactorRest/setValveStateTest/$valveIdentification/$valveState"(controller: 'nuclearReactorRest', action: 'setValveStateTest')
        "/nuclearReactorRest/setRodsStateTest/$rodsState"(controller: 'nuclearReactorRest', action: 'setRodsStateTest')
	}
}
