hibernate {
    cache.use_second_level_cache = true
    cache.use_query_cache = false
    //cache.region.factory_class = 'org.hibernate.cache.ehcache.EhCacheRegionFactory' // Hibernate 4
    cache.region.factory_class = 'org.hibernate.cache.ehcache.SingletonEhCacheRegionFactory' // Hibernate 4
    singleSession = true // configure OSIV singleSession mode
    flush.mode = 'manual' // OSIV session flush mode outside of transactional context
}

// environment specific settings
environments {
    development {
        dataSource {
            dbCreate = "create-drop" // one of 'create', 'create-drop', 'update', 'validate', ''
            url = "jdbc:postgresql://localhost:5432/reactornucleardev"
            driverClassName = "org.postgresql.Driver"
            username = "postgres"
            password = "admin"
            loggingSql = false
        }
    }
    test {
        dataSource {
            dbCreate = "update"
            url = "jdbc:postgresql://localhost:5432/reactornucleartest"
            driverClassName = "org.postgresql.Driver"
            username = "postgres"
            password = "admin"
        }
    }
    production {
        dataSource {
            dbCreate = "create-drop"
            url = "jdbc:postgresql://postgres8799-nuclearreactor.j.facilcloud.com:5432/reactornuclearprod"
            driverClassName = "org.postgresql.Driver"
            username = "webadmin"
            password = "VPPffv07235"

        }
    }
}
