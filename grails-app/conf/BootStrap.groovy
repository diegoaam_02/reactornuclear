import com.nuclearreactor.alerts.Alert
import com.nuclearreactor.alerts.AlertsConfiguration
import com.nuclearreactor.reactor.CoolantSystem
import com.nuclearreactor.reactor.HeaterState
import com.nuclearreactor.reactor.NuclearReactor
import com.nuclearreactor.reactor.PumpState
import com.nuclearreactor.reactor.RodsState
import com.nuclearreactor.reactor.Valve
import com.nuclearreactor.reactor.ValveState
import com.nuclearreactor.reactor.NuclearReactorConfiguration
import com.nuclearreactor.users.Role
import com.nuclearreactor.users.User
import com.nuclearreactor.users.UserRole

import static com.nuclearreactor.commons.entities.SystemConstants.ROLE_ADMIN
import static com.nuclearreactor.commons.entities.SystemConstants.ROLE_ADMIN_NUCLEAR_REACTOR
import static com.nuclearreactor.commons.entities.SystemConstants.ROLE_OPERATOR_NUCLEAR_REACTOR
import static com.nuclearreactor.commons.entities.SystemConstants.DEFAULT_ADMIN_EMAIL
import static com.nuclearreactor.commons.entities.SystemConstants.DEFAULT_ADM_NR_EMAIL
import static com.nuclearreactor.commons.entities.SystemConstants.GET_REACTOR_INFO_URL
import static com.nuclearreactor.commons.entities.SystemConstants.GET_TEMPERATURE_URL
import static com.nuclearreactor.commons.entities.SystemConstants.SET_HEATER_STATE_URL
import static com.nuclearreactor.commons.entities.SystemConstants.SET_PUMP_STATE_URL
import static com.nuclearreactor.commons.entities.SystemConstants.SET_VALVE_STATE_URL
import static com.nuclearreactor.commons.entities.SystemConstants.SET_RODS_STATE_URL

import com.nuclearreactor.core.users.*
import com.nuclearreactor.accounts.CompanyAccount
import com.nuclearreactor.accounts.AccountType

import grails.converters.JSON

class BootStrap {

    def init = { servletContext ->

        environments {
            development {
                createMarshellers()

                //DATA BASE POPULATION
                createDefaultRoles()
                createDefaultUsers()
                createDefaultAccounts()
                createDefaultNuclearReactor()

            }
            production {
                createMarshellers()

                //DATA BASE POPULATION
                createDefaultRoles()
                createDefaultUsers()
                createDefaultAccounts()
                createDefaultNuclearReactor()
            }
            test {

            }
        }
    }

    def createMarshellers = {
        JSON.registerObjectMarshaller( NuclearReactor ) { NuclearReactor nuclearReactor ->
            return [
                    code : nuclearReactor.code,
                    pumpState : nuclearReactor.pumpState,
                    rodsState : nuclearReactor.rodsState,
                    heaterState : nuclearReactor.heaterState,
                    lastUpdate : nuclearReactor.lastUpdate,
                    coolantSystem: nuclearReactor.coolantSystem,
                    valves: nuclearReactor.valves
            ]
        }
        JSON.registerObjectMarshaller( CoolantSystem ) { CoolantSystem coolantSystem ->
            return [
                    humidityPercentage : coolantSystem.humidityPercentage,
                    currentTemperature : coolantSystem.currentTemperature
                    //TODO: Include waterPressure when needed
            ]
        }
        JSON.registerObjectMarshaller( Valve ) { Valve valve ->
            return [
                    state : valve.state,
                    identifier : valve.identifier
            ]
        }
        JSON.registerObjectMarshaller( PumpState ) { PumpState pumpState ->
            return [
                    name : pumpState.name(),
                    value: pumpState.getState()
            ]
        }
        JSON.registerObjectMarshaller( ValveState ) { ValveState valveState ->
            return [
                    name : valveState.name(),
                    value: valveState.getState()
            ]
        }
        JSON.registerObjectMarshaller( RodsState ) { RodsState rodsState ->
            return [
                    name : rodsState.name(),
                    value: rodsState.getState()
            ]
        }
        JSON.registerObjectMarshaller( HeaterState ) { HeaterState heaterState ->
            return [
                    name : heaterState.name(),
                    value: heaterState.getState()
            ]
        }

        JSON.registerObjectMarshaller( Alert ) { Alert alert ->
            return [
                    type : alert.type.name(),
                    message: alert.message,
                    alertDate: alert.alertDate,
                    nuclearReactor: alert.nuclearReactor.id
            ]
        }
    }

    /**
     * Create Default Roles
     */
    def createDefaultRoles = {
        new Role(authority: ROLE_ADMIN).save(flush: true, failOnError: true)
        new Role(authority: ROLE_ADMIN_NUCLEAR_REACTOR).save(flush: true, failOnError: true)
        new Role(authority: ROLE_OPERATOR_NUCLEAR_REACTOR).save(flush: true, failOnError: true)
        return null
    }

    /**
     * Create Default Users
     */

    def createDefaultUsers(){
        def adminUser = new User(email: DEFAULT_ADMIN_EMAIL, password: 'Test1').save( flush: true, failOnError: true)
        def nuclearReactorCompany1 = new User(email: DEFAULT_ADM_NR_EMAIL, password: 'Test1').save( flush: true, failOnError: true)

        UserRole.create(adminUser, Role.findByAuthority(ROLE_ADMIN))
        UserRole.create(nuclearReactorCompany1, Role.findByAuthority(ROLE_ADMIN_NUCLEAR_REACTOR))

    }

    /**
     * Create Default Accounts
     */
    def createDefaultAccounts = {

            def adminAccount = new CompanyAccount(
                    firstName: 'Server Group',
                    lastName: 'Inf 1',
                    preferredLocale: 'en',
                    user: User.findByEmail(DEFAULT_ADMIN_EMAIL),
                    identificationNumber: '545454545454',
                    address: 'Calle falsa 123',
                    phoneNumber: '2344545',
                    accountType: AccountType.Principal
            ).save(flush: true, failOnError: true)
            def nuclearReactorAccount = new CompanyAccount(
                    firstName: 'Nuclear Reactor Company',
                    lastName: 'Inf 1',
                    preferredLocale: 'en',
                    user: User.findByEmail(DEFAULT_ADM_NR_EMAIL),
                    identificationNumber: '4343434',
                    address: 'Calle 312',
                    phoneNumber: '45345454',
                    accountType: AccountType.Customer
            ).save(flush: true, failOnError: true)

    }

    def createDefaultNuclearReactor = {
        def defaultCoolantSystem = new CoolantSystem(currentTemperature: 67, humidityPercentage: 0.2 , waterPressure: 20.2, idealTemperature: 20)
        def defaultNuclearReactor = new NuclearReactor(code: '2XD1AS', rodsState: RodsState.Up, coolantSystem: defaultCoolantSystem, company: CompanyAccount.findByUser(User.findByEmail(DEFAULT_ADM_NR_EMAIL)), pumpState: PumpState.Active, heaterState:  HeaterState.Active).save(flush: true, failOnError: true)
        def defaultValve1 = new Valve(state: ValveState.Open, identifier: "1", nuclearReactor:defaultNuclearReactor ).save(flush: true, failOnError: true)
        def defaultValve2 = new Valve(state: ValveState.Open, identifier: "2", nuclearReactor:defaultNuclearReactor).save(flush: true, failOnError: true)
        def defaultValve3 = new Valve(state: ValveState.Open, identifier: "3", nuclearReactor:defaultNuclearReactor).save(flush: true, failOnError: true)
        def defaultNuclarConfiguration = new NuclearReactorConfiguration(nuclearReactor: defaultNuclearReactor, getReactorInfoURL: GET_REACTOR_INFO_URL, getTemperatureURL: GET_TEMPERATURE_URL, setValveStateURL: SET_VALVE_STATE_URL, setRodsStateURL: SET_RODS_STATE_URL,setPumpStateURL: SET_PUMP_STATE_URL,setHeaterStateURL: SET_HEATER_STATE_URL )
        def defaultAlertConfiguration = new AlertsConfiguration(
                sendWebSocketAlert: true, sendEmailAlert: true,
                dangerMaxTempPercentage: 2.5, sendAlertToOperator: true,
                infoMaxTempPercentage: 1.5, warningMaxTempPercentage: 2.0,
                nuclearReactor: defaultNuclearReactor, sendAlertToAdministrator: true)
    }


    def destroy = {
    }
}
