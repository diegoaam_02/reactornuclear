package com.nuclearreactor.alerts.impl

import com.nuclearreactor.alerts.Alert
import com.nuclearreactor.alerts.AlertService
import com.nuclearreactor.alerts.AlertType
import com.nuclearreactor.alerts.AlertsConfiguration
import com.nuclearreactor.alerts.exceptions.AlertConfigurationNotSetException
import com.nuclearreactor.alerts.exceptions.AlertNotSendException
import com.nuclearreactor.commons.entities.JSONResponse
import com.nuclearreactor.commons.exceptions.MissingParameterException
import com.nuclearreactor.reactor.NuclearReactor
import com.nuclearreactor.reactor.exceptions.CoolantSystemNotFoundException
import com.nuclearreactor.util.alerts.AlertStrategyManager
import grails.converters.JSON
import grails.transaction.Transactional
import org.apache.log4j.Logger

import static com.nuclearreactor.commons.entities.SystemConstants.ALERTS_TOPIC

/**
 * Created by diegoamaya on 11/09/15.
 */
class AlertServiceImpl implements AlertService{

    AlertStrategyManager alertStrategyManager

    private final static String INFO_TEMP_MESSAGE = "INFO: The temperature is OK"
    private final static String WARN_TEMP_MESSAGE = "WARNING: The temperature started to be critic"
    private final static String DANGER_TEMP_MESSAGE = "DANGER: The temperature exceed the limit"
    private final static String DEFAULT_HTTP_METHOD = "POST"

    static Logger log = Logger.getLogger(AlertServiceImpl.class);

    @Override
    @Transactional(readOnly = false)
    def sendAlert(NuclearReactor nuclearReactor) throws MissingParameterException, AlertNotSendException, AlertConfigurationNotSetException{
        try{
            /**
             * WEB SOCKET ALERT
             */
            Alert alert = generateAlertFromNuclearReactor(nuclearReactor)
            alert.save(flush: true, failOnError: true)
            JSONResponse JSONToSend = new JSONResponse()
            JSONToSend.success = true
            JSONToSend.data = [:]
            JSONToSend.data.alert = alert
            String alertJSON = JSONToSend as JSON
            if(nuclearReactor.alertsConfiguration.sendWebSocketAlert){
                alertStrategyManager.sendWebSocketAlert(ALERTS_TOPIC,alertJSON)
                log.info("Alert Send to topic " + ALERTS_TOPIC + " with message " + alertJSON)
            }

            /**
             *  EMAIL ALERT
             */
            if(nuclearReactor.alertsConfiguration.sendEmailAlert) {

                alertStrategyManager.sendEmailAlert(nuclearReactor.company.user.email, alert)

            }


            /**
             * REST ALERT
             */
            if(nuclearReactor.alertsConfiguration.sendRestAlert){
                //Todo: put the correct link
                alertStrategyManager.sendRestAlert("",DEFAULT_HTTP_METHOD)
            }


        }catch (Exception e){
            throw new AlertNotSendException(e)
        }
    }

    @Override
    def generateAlertFromNuclearReactor(NuclearReactor nuclearReactor) throws MissingParameterException, AlertConfigurationNotSetException{
        Alert alert = null
        if(nuclearReactor){
            AlertsConfiguration alertsConfiguration = nuclearReactor.alertsConfiguration
            if(alertsConfiguration){
                if(nuclearReactor.coolantSystem){
                    alert = new Alert()
                    alert.nuclearReactor = nuclearReactor
                    Double idealTemperature = nuclearReactor.coolantSystem.idealTemperature
                    Double currentTemperature = nuclearReactor.coolantSystem.currentTemperature
                    Double currentRelationPercentage = currentTemperature/idealTemperature
                    /**
                     * Info Alert
                     */
                    if(currentRelationPercentage <= alertsConfiguration.infoMaxTempPercentage){
                        alert.setType(AlertType.Information)
                        alert.setMessage(INFO_TEMP_MESSAGE)
                    }
                    /**
                     * Warning Alert
                     */
                    else if(currentRelationPercentage <= alertsConfiguration.warningMaxTempPercentage){
                        alert.setType(AlertType.Warning)
                        alert.setMessage(WARN_TEMP_MESSAGE)
                    }
                    /**
                     * Danger Alert
                     */
                    else if(currentRelationPercentage <= alertsConfiguration.dangerMaxTempPercentage){
                        alert.setType(AlertType.Danger)
                        alert.setMessage(DANGER_TEMP_MESSAGE)
                    }
                }else{
                    throw new CoolantSystemNotFoundException()
                }
            }else{
                throw new AlertConfigurationNotSetException()
            }
        }else{
            throw new MissingParameterException()
        }
        alert
    }
}
