package com.nuclearreactor.alerts

import com.nuclearreactor.alerts.exceptions.AlertConfigurationNotSetException
import com.nuclearreactor.alerts.exceptions.AlertNotSendException
import com.nuclearreactor.commons.exceptions.MissingParameterException
import com.nuclearreactor.reactor.NuclearReactor

/**
 * Created by diegoamaya on 11/09/15.
 */
interface AlertService {

    def sendAlert(NuclearReactor nuclearReactor) throws MissingParameterException, AlertNotSendException, AlertConfigurationNotSetException

    def generateAlertFromNuclearReactor(NuclearReactor nuclearReactor) throws MissingParameterException, AlertConfigurationNotSetException
}