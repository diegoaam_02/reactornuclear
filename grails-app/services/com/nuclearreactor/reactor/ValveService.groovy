package com.nuclearreactor.reactor

import com.nuclearreactor.reactor.exceptions.NuclearRequestTimeOutException
import com.nuclearreactor.reactor.exceptions.SetValveStateURLNotSetException
import com.nuclearreactor.reactor.exceptions.ValveNotFoundException
import com.nuclearreactor.reactor.exceptions.ValveStateRequestTimeOutException

/**
 * Created by diegoamaya on 31/08/15.
 */
interface ValveService {

    def updateValveStateByIdentification(NuclearReactor nuclearReactor, String valveIdentification, ValveState valveState) throws ValveNotFoundException

    def setValveStateAndUpdateToRemote(NuclearReactor nuclearReactor, String valveIdentification, ValveState valveState) throws ValveNotFoundException, SetValveStateURLNotSetException, ValveStateRequestTimeOutException, NuclearRequestTimeOutException
}