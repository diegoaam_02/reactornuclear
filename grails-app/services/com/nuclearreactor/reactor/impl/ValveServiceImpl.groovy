package com.nuclearreactor.reactor.impl

import com.nuclearreactor.reactor.exceptions.NuclearRequestTimeOutException
import com.nuclearreactor.reactor.exceptions.SetValveStateURLNotSetException
import com.nuclearreactor.reactor.exceptions.ValveNotFoundException
import com.nuclearreactor.reactor.exceptions.ValveStateRequestTimeOutException
import com.nuclearreactor.reactor.NuclearReactor
import com.nuclearreactor.reactor.Valve
import com.nuclearreactor.reactor.ValveService
import com.nuclearreactor.reactor.ValveState
import com.nuclearreactor.reactor.restClient.ModelMonitorClientService
import grails.plugins.rest.client.RestResponse
import grails.transaction.Transactional
import org.codehaus.groovy.grails.web.json.JSONElement

/**
 * Created by diegoamaya on 31/08/15.
 */
class ValveServiceImpl implements ValveService{

    ModelMonitorClientService modelMonitorClientService

    @Override
    @Transactional(readOnly = false)
    def updateValveStateByIdentification(NuclearReactor nuclearReactor, String valveIdentification, ValveState valveState) throws ValveNotFoundException {
        boolean result = false
        if(valveIdentification && valveState){
            Valve valve = Valve.findByNuclearReactorAndIdentifier(nuclearReactor, valveIdentification)
            if(valve){
                if(!valve.state.equals(valveState)){
                    valve.state = valveState
                    valve.save(flush: true, failOnError: true)
                }
                result = true
            }else{
                throw new ValveNotFoundException()
            }
        }
        result
    }

    @Override
    @Transactional(readOnly = false)
    def setValveStateAndUpdateToRemote(NuclearReactor nuclearReactor, String valveIdentification, ValveState valveState) throws ValveNotFoundException, SetValveStateURLNotSetException, ValveStateRequestTimeOutException, NuclearRequestTimeOutException{
        boolean result = updateValveStateByIdentification(nuclearReactor, valveIdentification, valveState)
        if(result){
            /*
            TODO: IMPROVE DOUBLE QUERY ON VALVE TABLE
             */
            RestResponse restResponse =  modelMonitorClientService.setValveState(nuclearReactor, Valve.findByNuclearReactorAndIdentifier(nuclearReactor, valveIdentification), valveState)
            if(restResponse && restResponse.json){
                JSONElement reactorInfoOnJSON = restResponse.json
                if(reactorInfoOnJSON."mensaje" == 'OK'){
                    result = true
                }else{
                    throw new SetValveStateURLNotSetException()
                }
            }
        }
        result
    }
}
