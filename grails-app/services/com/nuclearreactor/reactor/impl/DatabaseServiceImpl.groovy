package com.nuclearreactor.reactor.impl

import com.nuclearreactor.audits.RequestHistory
import com.nuclearreactor.reactor.DatabaseService
import com.nuclearreactor.reactor.NuclearReactor
import com.nuclearreactor.reactor.exceptions.NuclearRequestTimeOutException
import grails.plugins.rest.client.RestBuilder
import grails.transaction.Transactional
import org.springframework.web.client.ResourceAccessException

/**
 * Created by hernan on 23/11/15.
 */
class DatabaseServiceImpl implements  DatabaseService{

    private static final log = org.apache.commons.logging.LogFactory.getLog(this)

    def restBuilder = new RestBuilder()

    @Override
    def pushLocalToRemote() {
//
//        def result
//
//        def link = "http://reactornuclear.j.facilcloud.com/NuclearReactor"+request

//        try{
//            result = restBuilder.get(link) {
//                accept "application/json"
//            }
        //       }catch (ResourceAccessException resourceAccessException){
        //           log.error resourceAccessException.message
        //           throw NuclearRequestTimeOutException()
        //       }catch(any){
        //          log.error any.message
        //      }

    }

    @Override
    @Transactional()
    def pullRemoteToLocal() {

        List<RequestHistory> requests

        RequestHistory requestHistory = new RequestHistory()
        requests = requestHistory.list()
        requestHistory.save()

        for(Iterator iterator = requests.iterator(); iterator.hasNext();){

            def requestStored = (RequestHistory) iterator.next()

            sendRequest(requestStored.getRequest(), requestStored.id)

        }

    }

    @Override
    @Transactional(readOnly = false)
    def storeRequest(String request, String requestType) {

        RequestHistory requestHistory = new RequestHistory()

        def currentTime = new Date();

        requestHistory.request= request
        requestHistory.requestDate=currentTime
        requestHistory.requestType=requestType
        requestHistory.save(flush: true, failOnError: true)

        log.info("Request Almacenado: "+request)

    }


    def checkStoredRequest(){

        def areRequest = false
        def request

        RequestHistory requests = new RequestHistory();
        request = requests.list()

        if(request.size()>0){
            areRequest=true
        }

        return areRequest

    }

    @Transactional()
    def sendRequest(String request, Long id){

        def requestHistory = RequestHistory.get(id)
        def urlServer = "http://reactornuclear.j.facilcloud.com/ReactorNuclear/nuclearReactorRest"
        //def urlServer = "http://localhost:8080/ReactorNuclear/nuclearReactorRest"

        log.info("Eliminando Request ")

        try{
            restBuilder.get(urlServer+request)
            requestHistory.delete(flush: true)
        }catch (ResourceAccessException resourceAccessException){
            throw new NuclearRequestTimeOutException(resourceAccessException)
        }
    }
}
