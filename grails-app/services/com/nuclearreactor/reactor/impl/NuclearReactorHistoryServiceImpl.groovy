package com.nuclearreactor.reactor.impl

import com.nuclearreactor.commons.exceptions.MissingParameterException
import com.nuclearreactor.reactor.NuclearReactor
import com.nuclearreactor.reactor.NuclearReactorHistory
import com.nuclearreactor.reactor.NuclearReactorHistoryService
import grails.converters.JSON

/**
 * Created by diegoamaya on 21/09/15.
 */
class NuclearReactorHistoryServiceImpl implements NuclearReactorHistoryService{

    def insertHistoryFromNuclearReactor(NuclearReactor nuclearReactor) throws MissingParameterException{
        if(nuclearReactor){
            NuclearReactorHistory history = new NuclearReactorHistory()
            history.nuclearReactor = nuclearReactor
            history.code = nuclearReactor.code
            history.lastUpdate = nuclearReactor.lastUpdate
            history.pumpState = nuclearReactor.pumpState
            history.rodsState = nuclearReactor.rodsState
            history.heaterState = nuclearReactor.heaterState
            history.date = new Date()
            history.humidityPercentage = nuclearReactor.coolantSystem.humidityPercentage
            history.currentTemperature = nuclearReactor.coolantSystem.currentTemperature
            history.idealTemperature = nuclearReactor.coolantSystem.idealTemperature
            history.valvesState = nuclearReactor.valves as JSON
            history.save(flush: true, failOnError: true)
        }else{
            throw new MissingParameterException()
        }
    }
}
