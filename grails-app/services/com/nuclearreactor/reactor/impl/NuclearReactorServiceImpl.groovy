package com.nuclearreactor.reactor.impl

import com.nuclearreactor.accounts.Account
import com.nuclearreactor.alerts.AlertService
import com.nuclearreactor.alerts.exceptions.AlertConfigurationNotSetException
import com.nuclearreactor.alerts.exceptions.AlertNotSendException
import com.nuclearreactor.commons.exceptions.MissingParameterException
import com.nuclearreactor.reactor.HeaterState
import com.nuclearreactor.reactor.NuclearReactorHistoryService
import com.nuclearreactor.reactor.exceptions.GetReactorInfoURLNotSetException
import com.nuclearreactor.reactor.exceptions.HeaterStateRequestTimeOutException
import com.nuclearreactor.reactor.exceptions.NuclearReactorNotFoundException
import com.nuclearreactor.reactor.exceptions.NuclearRequestTimeOutException
import com.nuclearreactor.reactor.exceptions.PumpStateRequestTimeOutException
import com.nuclearreactor.reactor.exceptions.RodsStateRequestTimeOutException
import com.nuclearreactor.reactor.exceptions.SetHeaterStateURLNotSetException
import com.nuclearreactor.reactor.exceptions.SetPumpStateURLNotSetException
import com.nuclearreactor.reactor.exceptions.SetRodsStateURLNotSetException
import com.nuclearreactor.reactor.NuclearReactor
import com.nuclearreactor.reactor.NuclearReactorService
import com.nuclearreactor.reactor.PumpState
import com.nuclearreactor.reactor.RodsState
import com.nuclearreactor.reactor.Valve
import com.nuclearreactor.reactor.ValveState
import com.nuclearreactor.reactor.restClient.ModelMonitorClientService
import grails.plugins.rest.client.RestResponse
import grails.transaction.Transactional
import org.apache.log4j.Logger
import org.codehaus.groovy.grails.web.json.JSONElement


/**
 *
 */
class NuclearReactorServiceImpl implements NuclearReactorService {


    ModelMonitorClientService modelMonitorClientService
    AlertService alertService
    NuclearReactorHistoryService nuclearReactorHistoryService

    static Logger log = Logger.getLogger(NuclearReactorServiceImpl.class);

    @Override
    @Transactional(readOnly = false)
    def updateTemperature(Account account, NuclearReactor nuclearReactor, Double currentTemperature) {
        def result = [:]
        result.success = false
        if(account && currentTemperature){
            if(nuclearReactor){
                nuclearReactor.coolantSystem.currentTemperature = currentTemperature
                nuclearReactor.coolantSystem.save(flush: true, failOnError: true)
                nuclearReactorHistoryService.insertHistoryFromNuclearReactor(nuclearReactor)
                result.success = true
            }
        }
        return result
    }

    /**
     * Updates all the reactors trying to make the REST request
     * and send alerts in case needed
     * @return
     */
    @Override
    @Transactional(readOnly = false)
    def updateAllNuclearReactorsFromRemote(boolean sendAlerts) {
        boolean result = false
        def nuclearReactors = NuclearReactor.findAll()
        if(nuclearReactors){
            nuclearReactors.each {
                try{
                    it = getReactorInfoAndUpdateFromRemote(it)
                    if(sendAlerts){
                        alertService.sendAlert(it)
                    }
                    result = true
                }catch (NuclearReactorNotFoundException nuclearReactorNotFound){
                    log.warn(nuclearReactorNotFound)
                }catch(GetReactorInfoURLNotSetException getReactorInfoNotSet) {
                    log.warn(getReactorInfoNotSet)
                }catch(NuclearRequestTimeOutException nuclearRequestTimeOut){
                    log.warn(nuclearRequestTimeOut)
                }catch(MissingParameterException missingParameterException){
                    log.warn(missingParameterException)
                }catch(AlertConfigurationNotSetException alertConfigException){
                    log.warn(alertConfigException)
                }catch(AlertConfigurationNotSetException alertConfigException){
                    log.warn(alertConfigException.printStackTrace())
                }catch(AlertNotSendException alertNotSend){
                    log.warn(alertNotSend)
                }
            }
        }
        return result
    }

    @Override
    @Transactional(readOnly = false)
    def getReactorInfoAndUpdateFromRemote(NuclearReactor nuclearReactor) throws NuclearReactorNotFoundException, GetReactorInfoURLNotSetException, NuclearRequestTimeOutException{
        if(nuclearReactor){
            RestResponse result = modelMonitorClientService.getReactorInformation(nuclearReactor)
            if(result && result.json){
                JSONElement reactorInfoOnJSON = result.json
                nuclearReactor.lastUpdate = new Date()
                nuclearReactor.rodsState = RodsState.fromStringToEnum(reactorInfoOnJSON."barras")
                nuclearReactor.pumpState = PumpState.fromStringToEnum(reactorInfoOnJSON."bomba")
                String humidityPercentageString = (String) reactorInfoOnJSON."temperaturaDTO"."valorHumedad"
                humidityPercentageString = humidityPercentageString?.substring(0,humidityPercentageString.length()-1)
                Float humidityPercentage = humidityPercentageString ? (Double.parseDouble(humidityPercentageString)/100).floatValue() : null
                String currentTemperatureString = (String) reactorInfoOnJSON."temperaturaDTO"."valorTemperatura"
                String identifier
                String state
                reactorInfoOnJSON."listaValvulas"?.each{
                    identifier = it.numero
                    state = it.estado
                    if(identifier && state){
                        Valve valve = Valve.findByNuclearReactorAndIdentifier(nuclearReactor, identifier)
                        if(valve){
                            ValveState valveState = ValveState.fromStringToEnum(state)
                            if(valveState){
                                valve.state = valveState
                                valve.save(flush: true, failOnError: true)
                            }
                        }
                    }

                }
                currentTemperatureString = currentTemperatureString?.substring(0,currentTemperatureString.length()-2)
                Double currentTemperature = currentTemperatureString ? (Double.parseDouble(currentTemperatureString)) : null
                nuclearReactor.coolantSystem.currentTemperature = currentTemperature
                nuclearReactor.coolantSystem.humidityPercentage = humidityPercentage
                nuclearReactor.coolantSystem.save(flush: true, failOnError: true)
                nuclearReactor.save(flush: true, failOnError: true)
                nuclearReactorHistoryService.insertHistoryFromNuclearReactor(nuclearReactor)
                return nuclearReactor;
            }
        }else{
            throw NuclearReactorNotFoundException()
        }
    }

    @Override
    @Transactional(readOnly = false)
    def getTemperatureAndUpdateFromRemote(NuclearReactor nuclearReactor) throws NuclearReactorNotFoundException, GetReactorInfoURLNotSetException, NuclearRequestTimeOutException{
        if(nuclearReactor){
            RestResponse result = modelMonitorClientService.getCurrentTemperature(nuclearReactor)
            if(result && result.json){
                JSONElement reactorInfoOnJSON = result.json
                nuclearReactor.lastUpdate = new Date()
                String humidityPercentageString = (String) reactorInfoOnJSON."valorHumedad"
                humidityPercentageString = humidityPercentageString?.substring(0,humidityPercentageString.length()-1)
                Float humidityPercentage = humidityPercentageString ? (Double.parseDouble(humidityPercentageString)/100).floatValue() : null
                String currentTemperatureString = (String) reactorInfoOnJSON."valorTemperatura"
                currentTemperatureString = currentTemperatureString?.substring(0,currentTemperatureString.length()-2)
                Double currentTemperature = currentTemperatureString ? (Double.parseDouble(currentTemperatureString)) : null
                nuclearReactor.coolantSystem.currentTemperature = currentTemperature
                nuclearReactor.coolantSystem.humidityPercentage = humidityPercentage
                nuclearReactor.coolantSystem.save(flush: true, failOnError: true)
                nuclearReactorHistoryService.insertHistoryFromNuclearReactor(nuclearReactor)
                return nuclearReactor.coolantSystem;
            }
        }else{
            throw NuclearReactorNotFoundException()
        }
    }

    @Override
    @Transactional(readOnly = false)
    def setRodsStateAndUpdateRemote(NuclearReactor nuclearReactor, RodsState rodsState) throws SetRodsStateURLNotSetException, RodsStateRequestTimeOutException, NuclearRequestTimeOutException, NuclearReactorNotFoundException{
        boolean result = updateRodsState(nuclearReactor, rodsState)
        if(result){
            /*
            TODO: IMPROVE DOUBLE QUERY ON VALVE TABLE
             */
            RestResponse restResponse =  modelMonitorClientService.setRodsState(nuclearReactor, rodsState)
            if(restResponse && restResponse.json){
                JSONElement reactorInfoOnJSON = restResponse.json
                if(reactorInfoOnJSON."mensaje" == 'OK'){
                    result = true
                }else{
                    throw new SetRodsStateURLNotSetException()
                }
            }
        }
        result
    }



    @Override
    @Transactional(readOnly = false)
    def updateRodsState(NuclearReactor nuclearReactor, RodsState rodsState) throws NuclearReactorNotFoundException{
        boolean result = false
        if(nuclearReactor){
            //if(!nuclearReactor.rodsState.equals(rodsState)){
            if (rodsState == RodsState.Down)
            {
                nuclearReactor.rodsState = "Down"
            }else
            {
                nuclearReactor.rodsState = "Up"
            }
                nuclearReactor.save(flush: true, failOnError: true)
                nuclearReactorHistoryService.insertHistoryFromNuclearReactor(nuclearReactor)
            //}
            result = true
        }else{
            throw new NuclearReactorNotFoundException()
        }
        result
    }


    @Override
    @Transactional(readOnly = false)
    def setPumpStateAndUpdateRemote(NuclearReactor nuclearReactor, PumpState pumpState) throws SetPumpStateURLNotSetException, PumpStateRequestTimeOutException, NuclearRequestTimeOutException, NuclearReactorNotFoundException{
        boolean result = updatePumpState(nuclearReactor, pumpState)
        if(result){
            /*
            TODO: IMPROVE DOUBLE QUERY ON VALVE TABLE
             */
            RestResponse restResponse =  modelMonitorClientService.setPumpState(nuclearReactor, pumpState)
            if(restResponse && restResponse.json){
                JSONElement reactorInfoOnJSON = restResponse.json
                if(reactorInfoOnJSON."mensaje" == 'OK'){
                    result = true
                }else{
                    throw new SetPumpStateURLNotSetException()
                }
            }
        }
        result
    }

    @Override
    @Transactional(readOnly = false)
    def updatePumpState(NuclearReactor nuclearReactor, PumpState pumpState) throws NuclearReactorNotFoundException{
        boolean result = false
        if(nuclearReactor){
            if(!nuclearReactor.pumpState.equals(pumpState)){
                nuclearReactor.pumpState = pumpState
                nuclearReactor.save(flush: true, failOnError: true)
                nuclearReactorHistoryService.insertHistoryFromNuclearReactor(nuclearReactor)
            }
            result = true
        }else{
            throw new NuclearReactorNotFoundException()
        }
        result
    }

    @Override
    @Transactional(readOnly = false)
    def setHeaterStateAndUpdateRemote(NuclearReactor nuclearReactor,HeaterState heaterState) throws SetHeaterStateURLNotSetException, HeaterStateRequestTimeOutException, NuclearRequestTimeOutException, NuclearReactorNotFoundException{
        boolean result = updateHeaterState(nuclearReactor, heaterState)
        if(result){
            /*
            TODO: IMPROVE DOUBLE QUERY ON VALVE TABLE
             */
            RestResponse restResponse =  modelMonitorClientService.setHeaterState(nuclearReactor, heaterState)
            if(restResponse && restResponse.json){
                JSONElement reactorInfoOnJSON = restResponse.json
                if(reactorInfoOnJSON."repuesta" == 'OK'){
                    result = true
                }else{
                    throw new SetHeaterStateURLNotSetException()
                }
            }
        }
        result
    }

    @Override
    @Transactional(readOnly = false)
    def updateHeaterState(NuclearReactor nuclearReactor, HeaterState heaterState) throws NuclearReactorNotFoundException{
        boolean result = false
        if(nuclearReactor){
            if(!nuclearReactor.heaterState.equals(heaterState)){
                nuclearReactor.heaterState = heaterState
                nuclearReactor.save(flush: true, failOnError: true)
                nuclearReactorHistoryService.insertHistoryFromNuclearReactor(nuclearReactor)
            }
            result = true
        }else{
            throw new NuclearReactorNotFoundException()
        }
        result
    }
}