package com.nuclearreactor.reactor

import com.nuclearreactor.accounts.Account
import com.nuclearreactor.reactor.HeaterState
import com.nuclearreactor.reactor.exceptions.GetReactorInfoURLNotSetException
import com.nuclearreactor.reactor.exceptions.HeaterStateRequestTimeOutException
import com.nuclearreactor.reactor.exceptions.NuclearReactorNotFoundException
import com.nuclearreactor.reactor.exceptions.NuclearRequestTimeOutException
import com.nuclearreactor.reactor.exceptions.PumpStateRequestTimeOutException
import com.nuclearreactor.reactor.exceptions.RodsStateRequestTimeOutException
import com.nuclearreactor.reactor.exceptions.SetHeaterStateURLNotSetException
import com.nuclearreactor.reactor.exceptions.SetPumpStateURLNotSetException
import com.nuclearreactor.reactor.exceptions.SetRodsStateURLNotSetException

/**
 * Provides the signatures ...
 */
interface NuclearReactorService {

    /**
     * Updates the temperatures to the reactor of the correspond account
     * @param account owner of the reactor
     * @param nuclearReactor nuclear reactor to be updated
     * @param currentTemperature current temperature
     * @return
     */
    def updateTemperature(Account account, NuclearReactor nuclearReactor, Double currentTemperature)

    /**
     * Updates all the reactors trying to make the REST request
     * and send alerts in case needed
     * @return
     */
    def updateAllNuclearReactorsFromRemote(boolean sendAlerts)

    /**
     *
     * @param nuclearReactor
     * @return
     * @throws NuclearReactorNotFoundException
     * @throws GetReactorInfoURLNotSetException
     * @throws NuclearRequestTimeOutException
     */
    def getReactorInfoAndUpdateFromRemote(NuclearReactor nuclearReactor) throws NuclearReactorNotFoundException, GetReactorInfoURLNotSetException, NuclearRequestTimeOutException

    /**
     *
     * @param nuclearReactor
     * @return
     * @throws NuclearReactorNotFoundException
     * @throws GetReactorInfoURLNotSetException
     * @throws NuclearRequestTimeOutException
     */
    def getTemperatureAndUpdateFromRemote(NuclearReactor nuclearReactor) throws NuclearReactorNotFoundException, GetReactorInfoURLNotSetException, NuclearRequestTimeOutException

    /**
     *
     * @param nuclearReactor
     * @param rodsState
     * @return
     * @throws SetRodsStateURLNotSetException
     * @throws RodsStateRequestTimeOutException
     * @throws NuclearRequestTimeOutException
     * @throws NuclearReactorNotFoundException
     */
    def setRodsStateAndUpdateRemote(NuclearReactor nuclearReactor, RodsState rodsState) throws SetRodsStateURLNotSetException, RodsStateRequestTimeOutException, NuclearRequestTimeOutException, NuclearReactorNotFoundException

    /**
     *
     * @param nuclearReactor
     * @param rodsState
     * @return
     * @throws NuclearReactorNotFoundException
     */
    def updateRodsState(NuclearReactor nuclearReactor, RodsState rodsState) throws NuclearReactorNotFoundException

    /**
     *
     * @param nuclearReactor
     * @param pumpState
     * @return
     * @throws SetPumpStateURLNotSetException
     * @throws PumpStateRequestTimeOutException
     * @throws NuclearRequestTimeOutException
     * @throws NuclearReactorNotFoundException
     */
    def setPumpStateAndUpdateRemote(NuclearReactor nuclearReactor, PumpState pumpState) throws SetPumpStateURLNotSetException, PumpStateRequestTimeOutException, NuclearRequestTimeOutException, NuclearReactorNotFoundException

    /**
     *
     * @param nuclearReactor
     * @param pumpState
     * @return
     * @throws NuclearReactorNotFoundException
     */
   def updatePumpState(NuclearReactor nuclearReactor, PumpState pumpState) throws NuclearReactorNotFoundException

   def setHeaterStateAndUpdateRemote(NuclearReactor nuclearReactor, HeaterState heaterState) throws SetHeaterStateURLNotSetException, HeaterStateRequestTimeOutException, NuclearRequestTimeOutException, NuclearReactorNotFoundException

    /***
     *
     * @param nuclearReactor
     * @param heaterState
     * @return
     * @throws NuclearReactorNotFoundException
     */
    def updateHeaterState(NuclearReactor nuclearReactor, HeaterState heaterState) throws NuclearReactorNotFoundException
}
