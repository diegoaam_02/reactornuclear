package com.nuclearreactor.reactor.restClient

import com.nuclearreactor.reactor.HeaterState
import com.nuclearreactor.reactor.PumpState
import com.nuclearreactor.reactor.exceptions.GetReactorInfoURLNotSetException
import com.nuclearreactor.reactor.exceptions.GetTemperatureURLNotSetException
import com.nuclearreactor.reactor.exceptions.HeaterStateRequestTimeOutException
import com.nuclearreactor.reactor.exceptions.NuclearReactorNotFoundException
import com.nuclearreactor.reactor.exceptions.NuclearRequestTimeOutException
import com.nuclearreactor.reactor.exceptions.PumpStateRequestTimeOutException
import com.nuclearreactor.reactor.exceptions.RodsStateRequestTimeOutException
import com.nuclearreactor.reactor.exceptions.SetHeaterStateURLNotSetException
import com.nuclearreactor.reactor.exceptions.SetPumpStateURLNotSetException
import com.nuclearreactor.reactor.exceptions.SetRodsStateURLNotSetException
import com.nuclearreactor.reactor.exceptions.SetValveStateURLNotSetException
import com.nuclearreactor.reactor.exceptions.ValveStateRequestTimeOutException
import com.nuclearreactor.reactor.NuclearReactor
import com.nuclearreactor.reactor.Valve
import com.nuclearreactor.reactor.RodsState
import com.nuclearreactor.reactor.ValveState

/**
 * Created by diegoamaya on 26/08/15.
 */
interface ModelMonitorClientService {

    def getCurrentTemperature(NuclearReactor nuclearReactor) throws NuclearReactorNotFoundException, GetTemperatureURLNotSetException, NuclearRequestTimeOutException

    def getReactorInformation(NuclearReactor nuclearReactor) throws NuclearReactorNotFoundException, GetReactorInfoURLNotSetException, NuclearRequestTimeOutException

    def setValveState(NuclearReactor nuclearReactor, Valve valve, ValveState valveState) throws SetValveStateURLNotSetException, ValveStateRequestTimeOutException, NuclearRequestTimeOutException

    def setRodsState(NuclearReactor nuclearReactor, RodsState rodsState) throws SetRodsStateURLNotSetException, RodsStateRequestTimeOutException , NuclearRequestTimeOutException

    def setPumpState(NuclearReactor nuclearReactor, PumpState pumpState) throws SetPumpStateURLNotSetException, PumpStateRequestTimeOutException , NuclearRequestTimeOutException

    def setHeaterState(NuclearReactor nuclearReactor, HeaterState heaterState) throws SetHeaterStateURLNotSetException, HeaterStateRequestTimeOutException , NuclearRequestTimeOutException


}