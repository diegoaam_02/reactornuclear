package com.nuclearreactor.reactor.restClient.impl

import com.nuclearreactor.reactor.HeaterState
import com.nuclearreactor.reactor.PumpState
import com.nuclearreactor.reactor.exceptions.GetReactorInfoURLNotSetException
import com.nuclearreactor.reactor.exceptions.GetTemperatureURLNotSetException
import com.nuclearreactor.reactor.exceptions.HeaterStateRequestTimeOutException
import com.nuclearreactor.reactor.exceptions.NuclearReactorNotFoundException
import com.nuclearreactor.reactor.exceptions.NuclearRequestTimeOutException
import com.nuclearreactor.reactor.exceptions.PumpStateRequestTimeOutException
import com.nuclearreactor.reactor.exceptions.RodsStateRequestTimeOutException
import com.nuclearreactor.reactor.exceptions.SetHeaterStateURLNotSetException
import com.nuclearreactor.reactor.exceptions.SetPumpStateURLNotSetException
import com.nuclearreactor.reactor.exceptions.SetRodsStateURLNotSetException
import com.nuclearreactor.reactor.exceptions.SetValveStateURLNotSetException
import com.nuclearreactor.reactor.exceptions.ValveStateRequestTimeOutException
import com.nuclearreactor.reactor.NuclearReactor
import com.nuclearreactor.reactor.RodsState
import com.nuclearreactor.reactor.Valve
import com.nuclearreactor.reactor.ValveState
import com.nuclearreactor.reactor.restClient.ModelMonitorClientService
import grails.plugins.rest.client.RestBuilder
import jline.internal.Log
import org.springframework.web.client.ResourceAccessException

import java.text.MessageFormat

/**
 * Created by diegoamaya on 27/08/15.
 */
class ModelMonitorClientServiceImpl implements ModelMonitorClientService{

    RestBuilder restBuilder
    private static final log = org.apache.commons.logging.LogFactory.getLog(this)

    @Override
    def getCurrentTemperature(NuclearReactor nuclearReactor)  throws NuclearReactorNotFoundException, GetTemperatureURLNotSetException, NuclearRequestTimeOutException{
        def result
        if(nuclearReactor && nuclearReactor.configuration){
            if(nuclearReactor.configuration.getReactorInfoURL){
                try{
                    result = restBuilder.get(nuclearReactor.configuration?.getTemperatureURL) {
                        accept "application/json"
                        header 'token','0f5f884f-63cc-4367-a25a-6b94591bdf15'
                    }
                }catch (ResourceAccessException resourceAccessException){
                    log.error resourceAccessException.message
                    throw NuclearRequestTimeOutException()
                }catch(any){
                    log.error any.message
                }
            }else{
                throw new GetTemperatureURLNotSetException()
            }
        }else{
            throw new NuclearReactorNotFoundException()
        }
        result
    }

    @Override
    def getReactorInformation(NuclearReactor nuclearReactor) throws NuclearReactorNotFoundException, GetReactorInfoURLNotSetException, NuclearRequestTimeOutException{
        def result
        if(nuclearReactor && nuclearReactor.configuration){
            if(nuclearReactor.configuration.getReactorInfoURL){
                try{
                    log.info 'Sending get to '+nuclearReactor.configuration.getReactorInfoURL
                    result = restBuilder.get(nuclearReactor.configuration?.getReactorInfoURL) {
                        header 'token','0f5f884f-63cc-4367-a25a-6b94591bdf15'
                        accept "application/json"
                    }
                }catch (ResourceAccessException resourceAccessException){
                    log.error resourceAccessException
                    throw new NuclearRequestTimeOutException(resourceAccessException)
                }catch(Exception any){
                    log.error any
                }
            }else{
                throw new GetReactorInfoURLNotSetException()
            }
        }else{
            throw new NuclearReactorNotFoundException()
        }
        result
    }

    @Override
    def setValveState(NuclearReactor nuclearReactor, Valve valve, ValveState valveState) throws SetValveStateURLNotSetException, ValveStateRequestTimeOutException, NuclearRequestTimeOutException {
        def result
        if(nuclearReactor && nuclearReactor.configuration){
            if(nuclearReactor.configuration.setValveStateURL){
                String link = getValveTransformedLink(nuclearReactor.configuration.setValveStateURL, valve.identifier, valveState.getState())
                try{
                    result = restBuilder.get(link) {
                        accept "application/json"
                        header 'token','0f5f884f-63cc-4367-a25a-6b94591bdf15'
                    }
                    Log.info("Request: " + link)
                    Log.info("Response: "+result.json)
                }catch (ResourceAccessException resourceAccessException){
                    log.info resourceAccessException.message
                    throw new ValveStateRequestTimeOutException()
                }catch(any){
                    log.error any.message
                }
            }else{
                throw new SetValveStateURLNotSetException()
            }
        }else{
            throw new NuclearReactorNotFoundException()
        }
        result
    }

    @Override
    def setRodsState(NuclearReactor nuclearReactor, RodsState rodsState) throws SetRodsStateURLNotSetException, RodsStateRequestTimeOutException , NuclearRequestTimeOutException {
        def result
        if(nuclearReactor && nuclearReactor.configuration){
            if(nuclearReactor.configuration.setRodsStateURL){
                String modelRodsState = rodsState.equals(RodsState.Down) ? "barrasD" : "barrasU"
                String link = getValveTransformedLink(nuclearReactor.configuration.setRodsStateURL, modelRodsState)
                try{
                    result = restBuilder.get(link) {
                        accept "application/json"
                        header 'token','0f5f884f-63cc-4367-a25a-6b94591bdf15'
                    }
                    Log.info("Request: "+link)
                    Log.info("Response: "+result.json)
                }catch (ResourceAccessException resourceAccessException){
                    log.info resourceAccessException.message
                    throw new RodsStateRequestTimeOutException()
                }catch(any){
                    log.error any.message
                }
            }else{
                throw new SetRodsStateURLNotSetException()
            }
        }else{
            throw new NuclearReactorNotFoundException()
        }
        result
    }

    @Override
    def setPumpState(NuclearReactor nuclearReactor, PumpState pumpState) throws SetPumpStateURLNotSetException, PumpStateRequestTimeOutException , NuclearRequestTimeOutException {
        def result
        if(nuclearReactor && nuclearReactor.configuration){
            if(nuclearReactor.configuration.setPumpStateURL){
                String link = getValveTransformedLink(nuclearReactor.configuration.setPumpStateURL, pumpState.getState())
                try{
                    result = restBuilder.get(link) {
                        accept "application/json"
                        header 'token','0f5f884f-63cc-4367-a25a-6b94591bdf15'
                    }
                    Log.info("Request: "+link)
                    Log.info("Response: "+result.json)
                }catch (ResourceAccessException resourceAccessException){
                    log.info resourceAccessException.message
                    throw new PumpStateRequestTimeOutException()
                }catch(any){
                    log.error any.message
                }
            }else{
                throw new SetPumpStateURLNotSetException()
            }
        }else{
            throw new NuclearReactorNotFoundException()
        }
        result
    }

    @Override
    def setHeaterState(NuclearReactor nuclearReactor, HeaterState heaterState) throws SetHeaterStateURLNotSetException, HeaterStateRequestTimeOutException , NuclearRequestTimeOutException {
        def result
        if(nuclearReactor && nuclearReactor.configuration){
            if(nuclearReactor.configuration.setHeaterStateURL){
                String link = getValveTransformedLink(nuclearReactor.configuration.setHeaterStateURL, heaterState.getState())
                try{
                    result = restBuilder.get(link) {
                        accept "application/json"
                        header 'token','0f5f884f-63cc-4367-a25a-6b94591bdf15'
                    }
                    Log.info("Request: "+link)
                    Log.info("Response: "+result.json)
                }catch (ResourceAccessException resourceAccessException){
                    log.info resourceAccessException.message
                    throw new HeaterStateRequestTimeOutException()
                }catch(any){
                    log.error any.message
                }
            }else{
                throw new SetHeaterStateURLNotSetException()
            }
        }else{
            throw new NuclearReactorNotFoundException()
        }
        result
    }


    private def getValveTransformedLink(String link, String... params){
        return MessageFormat.format(link,params);
    }
}
