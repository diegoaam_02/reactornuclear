package com.nuclearreactor.reactor

/**
 * Created by hernan on 23/11/15.
 */
interface DatabaseService {

    /**
     * Push the Local request to the Remote Server
     *
     * @return
     */
    def pushLocalToRemote();

    /**
     *  Pull the Remote Request to the Local Server
     *
     * @return
     */
    def pullRemoteToLocal();

    /**
     * Store the every request in the Database remote
     *
     * @return
     */
    def storeRequest(String request, String requestType);

}
