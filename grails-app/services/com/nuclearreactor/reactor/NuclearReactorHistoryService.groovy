package com.nuclearreactor.reactor

import com.nuclearreactor.commons.exceptions.MissingParameterException

/**
 * Created by diegoamaya on 21/09/15.
 */
interface NuclearReactorHistoryService {

    def insertHistoryFromNuclearReactor(NuclearReactor nuclearReactor) throws MissingParameterException

}
