package com.nuclearreactor.reactor.rest

import com.nuclearreactor.reactor.HeaterState
import com.nuclearreactor.reactor.PumpState
import com.nuclearreactor.reactor.Valve
import com.nuclearreactor.reactor.exceptions.GetReactorInfoURLNotSetException
import com.nuclearreactor.reactor.exceptions.HeaterStateRequestTimeOutException
import com.nuclearreactor.reactor.exceptions.NuclearReactorNotFoundException
import com.nuclearreactor.reactor.exceptions.NuclearRequestTimeOutException
import com.nuclearreactor.reactor.exceptions.PumpStateRequestTimeOutException
import com.nuclearreactor.reactor.exceptions.RodsStateRequestTimeOutException
import com.nuclearreactor.reactor.exceptions.SetHeaterStateURLNotSetException
import com.nuclearreactor.reactor.exceptions.SetPumpStateURLNotSetException
import com.nuclearreactor.reactor.exceptions.SetRodsStateURLNotSetException
import com.nuclearreactor.reactor.exceptions.SetValveStateURLNotSetException
import com.nuclearreactor.reactor.exceptions.ValveNotFoundException
import com.nuclearreactor.reactor.exceptions.ValveStateRequestTimeOutException
import com.nuclearreactor.reactor.NuclearReactor
import com.nuclearreactor.reactor.RodsState
import com.nuclearreactor.reactor.ValveState
import com.nuclearreactor.commons.entities.JSONResponse
import grails.converters.JSON
import grails.rest.RestfulController
import grails.transaction.Transactional
import groovy.json.JsonSlurper

@Transactional(readOnly = false)
class NuclearReactorRestController extends RestfulController {

    static responseFormats = ['json', 'xml']

    static allowedMethods = [
            getReactorInformation: 'GET',
            getCurrentTemperature: 'GET',
            setValveState: 'POST',
            setRodsState: 'POST',
            setPumpState: 'POST',
            setHeaterState: 'POST',

            /**
             * JUST FOR TEST PURPOSES
             */
            setValveStateTest: 'GET',
            mockGetReactorInformation: 'GET'
    ]

    static nuclearReactorService
    static valveService
    static alertService
    def databaseService

    public boolean validateAccess(String user, String validationKey){
        String temp = user+"4nDro1D";
        String hashFromContent = temp.encodeAsMD5();
        if (validationKey == hashFromContent)
        {
            return true
        }else
        {
            return false
        }
    }


    public boolean pingServiceMovil(String url){

        def status=false;

        try {
            URL siteURL = new URL(url)
            HttpURLConnection connection = (HttpURLConnection) siteURL.openConnection();
            connection.setRequestMethod("GET")
            connection.connect();

            int code = connection.getResponseCode()
            if(code==200){
                status=true
            }
        }catch (Exception err){
            status=false
        }

        return status
    }


    /**
     * code =
     * @return
     * {
     *     "success": Boolean,
     *     "message": String,
     *     data:{
     *          nuclearReactor:{
     *
     *              valveList:[
     *                  {
     *                      id: 1,
     *                      state:ON
     *                  },
     *                  {
     *                      id: 2,
     *                      state:OFF
     *                  },
     *                  {
     *                      id: 3,
     *                      state:OFF
     *                  }
     *              ]
     *              temperature : {
     *                  currentTemperature : Double, Float,
     *                  humidity: Double
     *
     *              }
     *          }
     *
     *     }
     *
     *
     * }
     */
    //TODO: Bind code as method argument
    def getReactorInformation(String code, String user, String validationKey) {

        def jsonResponse = new JSONResponse()
        if (validateAccess(user, validationKey))
        {
            if(code){
                NuclearReactor nuclearReactor = NuclearReactor.findByCode(code)
                if(nuclearReactor){
                    try{
                        nuclearReactor = nuclearReactorService.getReactorInfoAndUpdateFromRemote(nuclearReactor)
                        def alert = alertService.generateAlertFromNuclearReactor(nuclearReactor)
                        alert.save(flush: true, failOnError: true)
                        jsonResponse.success = true
                        jsonResponse.message = 'Success'
                        jsonResponse.data = [:]
                        jsonResponse.data.put("nuclearReactor", nuclearReactor)
                        jsonResponse.data.put("alert", alert)


                    }catch (NuclearReactorNotFoundException nuclearReactorNotFound){
                        jsonResponse.message = "Nuclear reactor was not found"
                    }catch (GetReactorInfoURLNotSetException getReactorInfoURLNotSet){
                        jsonResponse.message = "The URL for getting the reactor information has not been set"
                    }catch (NuclearRequestTimeOutException nuclearRequestTimeOut){
                        def alert = alertService.generateAlertFromNuclearReactor(nuclearReactor)
                        alert.save(flush: true, failOnError: true)
                        jsonResponse.success = false
                        jsonResponse.data = [:]
                        jsonResponse.data.put("nuclearReactor", nuclearReactor)
                        jsonResponse.data.put("alert", alert)
                        jsonResponse.message = "The model of the reactor does not respond (time out)"
                    }
                }else {
                    jsonResponse.message = "Nuclear reactor was not found"
                }
            }else{
                jsonResponse.message = "You must provide the code of the reactor"
            }
        }else
        {
            jsonResponse.success = false
            jsonResponse.message = "Invalid Credentials"
        }

        log.debug('sent response: ' + (jsonResponse as JSON))
        render jsonResponse as JSON
        return
    }

    /**
     *
     * @param code
     * @return
     */
    def getCurrentTemperature(String code, String user, String validationKey) {
        def jsonResponse = new JSONResponse()

        if (validateAccess(user, validationKey))
        {
            if(code){
                NuclearReactor nuclearReactor = NuclearReactor.findByCode(code)
                if(nuclearReactor){
                    try{
                        def coolantSystem = nuclearReactorService.getTemperatureAndUpdateFromRemote(nuclearReactor)
                        jsonResponse.success = true
                        jsonResponse.message = 'Success'
                        jsonResponse.data = [:]
                        jsonResponse.data.put("coolantSystem", coolantSystem)
                    }catch (NuclearReactorNotFoundException nuclearReactorNotFound){
                        jsonResponse.message = "Nuclear reactor was not found"
                    }catch (GetReactorInfoURLNotSetException getReactorInfoURLNotSet){
                        jsonResponse.message = "The URL for getting the reactor information has not been set"
                    }catch (NuclearRequestTimeOutException nuclearRequestTimeOut){
                        jsonResponse.success = false
                        jsonResponse.data = [:]
                        jsonResponse.data.put("coolantSystem", nuclearReactor.coolantSystem)
                        jsonResponse.message = "The model of the reactor does not respond (time out)"
                    }
                }else {
                    jsonResponse.message = "Nuclear reactor was not found"
                }
            }else{
                jsonResponse.message = "You must provide the code of the reactor"
            }
        }else
        {
            jsonResponse.success = false
            jsonResponse.message = "Invalid Credentials"
        }
        respond jsonResponse
    }

    /**
     *
     * reactorCode
     * @param idValve
     * @return
     */
    def setValveState(String reactorCode, String valveIdentification, String valveState, String user, String validationKey){

        def jsonResponse = new JSONResponse()
        if (validateAccess(user, validationKey))
        {
            ValveState valveStateTransformed = ValveState.fromStringToEnum(valveState)
            if(reactorCode && valveIdentification && valveStateTransformed){
                NuclearReactor nuclearReactor = NuclearReactor.findByCode(reactorCode)
                if(nuclearReactor){
                    try{
                        boolean result = changeValve(nuclearReactor, valveIdentification, valveStateTransformed)
                        jsonResponse.success = result
                        jsonResponse.message = result ? 'Success' : 'Valve not updated'

                    }catch (ValveNotFoundException valveNotFound){
                        jsonResponse.message = "The valve was not found"
                    }catch (ValveStateRequestTimeOutException timeOut){
                        jsonResponse.message = "Time out"
                        log.info("Almacenando el Valve Request ")
                        def request = "/setValveState?reactorCode="+reactorCode+"&valveIdentification="+valveIdentification+"&user="+user+"&validationKey="+validationKey
                        databaseService.storeRequest(request, "valveRequest")
                    }catch (any){
                        log.error any.message
                        jsonResponse.message = any.message
                    }
                }else {
                    jsonResponse.message = "Nuclear reactor was not found"
                }
            }else{
                jsonResponse.message = "You must provide the code of the reactor, valveState and valve identification"
            }
        }else
        {
            jsonResponse.success = false
            jsonResponse.message = "Invalid Credentials"
        }
        respond jsonResponse
    }

    /***
     *
     * @param reactorCode
     * @param rodsState
     * @return
     */
    def setRodsState(String reactorCode, String rodsState, String user, String validationKey){
        def jsonResponse = new JSONResponse()
        if (validateAccess(user, validationKey))
        {
            if (rodsState == 'ON')
                rodsState = 'Arriba'
            else if (rodsState == 'OFF')
                rodsState = 'Abajo'

            RodsState rodsStateTransformed = RodsState.fromStringToEnum((String) rodsState)
            if(reactorCode && rodsStateTransformed){
                NuclearReactor nuclearReactor = NuclearReactor.findByCode(reactorCode)
                if(nuclearReactor){
                    try{
                        boolean result = nuclearReactorService.setRodsStateAndUpdateRemote(nuclearReactor, rodsStateTransformed)
                        ValveState valveStateTransformed = rodsStateTransformed == RodsState.Down ? ValveState.Closed : ValveState.Open

                        changeValve(nuclearReactor, "2", valveStateTransformed)
                        changeValve(nuclearReactor, "3", valveStateTransformed)
                        HeaterState heaterState = rodsStateTransformed == RodsState.Down ?HeaterState.Inactive:HeaterState.Active;
                        changeHeater(nuclearReactor, heaterState)
                        jsonResponse.success = result
                        jsonResponse.message = result ? 'Success' : 'Rods not updated'
                        jsonResponse.data = 'RS='+ rodsStateTransformed + ';VS=' + valveStateTransformed


                    }catch (SetRodsStateURLNotSetException notFound){
                        jsonResponse.message = notFound.message
                    }catch (RodsStateRequestTimeOutException timeOut){
                        jsonResponse.message = "Time out"
                        log.info("Almacenando el Rods Request ")
                        def request = "/setRodsState?reactorCode="+reactorCode+"&rodsState="+rodsState+"&user="+user+"&validationKey="+validationKey
                        databaseService.storeRequest(request, "RodsRequest")
                    }catch (any){
                        log.error any.message
                        jsonResponse.message = any.message
                    }
                }else {
                    jsonResponse.message = "Nuclear reactor was not found"
                }
            }else{
                jsonResponse.message = "You must provide the code of the reactor"
            }
        }else
        {
            jsonResponse.success = false
            jsonResponse.message = "Invalid Credentials"
        }
        respond jsonResponse
    }

    /**
     *
     * @param reactorCode
     * @param pumpState
     * @return
     */
    def setPumpState(String reactorCode, String pumpState, String user, String validationKey){
        def jsonResponse = new JSONResponse()
        if (validateAccess(user, validationKey))
        {
        PumpState pumpStateTransformed = PumpState.fromStringToEnum((String) pumpState)

        if(reactorCode && pumpStateTransformed){
            NuclearReactor nuclearReactor = NuclearReactor.findByCode(reactorCode)
            if(nuclearReactor){
                try{
                    boolean result = changePump(nuclearReactor, pumpStateTransformed)
                    jsonResponse.success = result
                    jsonResponse.message = result ? 'Success' : 'Pump not updated'
                }catch (SetPumpStateURLNotSetException notFound){
                    jsonResponse.message = notFound.message
                }catch (PumpStateRequestTimeOutException timeOut){
                    jsonResponse.message = "Time out"
                }catch (any){
                    log.error any.message
                    jsonResponse.message = any.message
                }
            }else {
                jsonResponse.message = "Nuclear reactor was not found"
            }
        }else{
            jsonResponse.message = "You must provide the code of the reactor"
        }
        }else
        {
            jsonResponse.success = false
            jsonResponse.message = "Invalid Credentials"
        }
        respond jsonResponse
    }

    /**
     *
     * @param reactorCode
     * @param heaterState
     * @return
     */
        def setHeaterState(String reactorCode, String heaterState, String user, String validationKey){
        def jsonResponse = new JSONResponse()
        if (validateAccess(user, validationKey))
        {
        HeaterState heaterStateTransformed = HeaterState.fromStringToEnum((String) heaterState)
        if(reactorCode && heaterStateTransformed){
            NuclearReactor nuclearReactor = NuclearReactor.findByCode(reactorCode)
            if(nuclearReactor){
                try{
                    boolean result = nuclearReactorService.setHeaterStateAndUpdateRemote (nuclearReactor, heaterStateTransformed)
                    jsonResponse.success = result
                    jsonResponse.message = result ? 'Success' : 'Heater not updated'
                }catch (SetHeaterStateURLNotSetException notFound){
                    jsonResponse.message = notFound.message
                }catch (HeaterStateRequestTimeOutException timeOut){
                    jsonResponse.message = "Time out"
                }catch (any){
                    log.error any.message
                    jsonResponse.message = any.message
                }
            }else {
                jsonResponse.message = "Nuclear reactor was not found"
            }
        }else{
            jsonResponse.message = "You must provide the code of the reactor"
        }
        }else
        {
            jsonResponse.success = false
            jsonResponse.mes    sage = "Invalid Credentials"
        }
        respond jsonResponse
    }


    /**
     *
     * @param nuclearReactor
     * @param pumpStateTransformed
     * @return
     * @throws SetPumpStateURLNotSetException
     * @throws PumpStateRequestTimeOutException
     * @throws NuclearRequestTimeOutException
     */
    def private changePump(NuclearReactor nuclearReactor,PumpState pumpStateTransformed)throws SetPumpStateURLNotSetException, PumpStateRequestTimeOutException, NuclearRequestTimeOutException{
        boolean result = nuclearReactorService.setPumpStateAndUpdateRemote (nuclearReactor, pumpStateTransformed)
        result;
    }

    /**
     *
     * @param nuclearReactor
     * @param valveIdentification
     * @param valveStateTransformed
     * @return
     * @throws ValveNotFoundException
     * @throws SetValveStateURLNotSetException
     * @throws ValveStateRequestTimeOutException
     * @throws NuclearRequestTimeOutException
     */
    def private changeValve(NuclearReactor nuclearReactor,String valveIdentification,ValveState valveStateTransformed)throws ValveNotFoundException, SetValveStateURLNotSetException, ValveStateRequestTimeOutException, NuclearRequestTimeOutException{

        boolean result = valveService.setValveStateAndUpdateToRemote(nuclearReactor,valveIdentification,valveStateTransformed)
        if(valveIdentification=="1"){
            //Active o Inactive the pump depending of the valve state
            PumpState pumpState = valveStateTransformed.state.equals("ON")?PumpState.Active:PumpState.Inactive
            changePump(nuclearReactor,pumpState)
        }
        result;
    }

    /***
     *
     * @param nuclearReactor
     * @param heaterStateTransformed
     * @return
     * @throws SetHeaterStateURLNotSetException
     * @throws HeaterStateRequestTimeOutException
     * @throws NuclearRequestTimeOutException
     */
    def private changeHeater(NuclearReactor nuclearReactor,HeaterState heaterStateTransformed)throws SetHeaterStateURLNotSetException, HeaterStateRequestTimeOutException, NuclearRequestTimeOutException{
        boolean result = nuclearReactorService.setHeaterStateAndUpdateRemote (nuclearReactor, heaterStateTransformed)
        result;
    }

    /***
     * JUST FOR TEST PURPOSES
     */

    def setValveStateTest(){
        def result = [:]
        result.mensaje = 'OK'
        render result as JSON
        return
    }

    def setRodsStateTest(){
        def result = [:]
        result.mensaje = 'OK'
        render result as JSON
        return
    }

    def mockGetReactorInformation(String code) {
        String jso ="{"
        NuclearReactor nuclearReactor = NuclearReactor.findByCode(code)
        if(nuclearReactor){
            try {

                if (nuclearReactor.getRodsState().toString().equals("Up")) {
                    jso += "\"barra\":\"ON\","
                } else {
                    jso += "\"barra\":\"OFF\","
                }

                if (nuclearReactor.getPumpState().toString().equals("Active")) {
                    jso += "\"bomba\":\"ON\","
                } else {
                    jso += "\"bomba\":\"OFF\","
                }


                jso += "\"listaValvulas\":["
                Valve valve = Valve.findByNuclearReactorAndIdentifier(nuclearReactor, "1")
                if (valve.getState().toString().equals("Open")) {
                    jso += "{\"estado\":\"ON\",\"numeroValvula\":\"1\"},"
                } else {
                    jso += "{\"estado\":\"OFF\",\"numeroValvula\":\"1\"},"
                }

                valve = Valve.findByNuclearReactorAndIdentifier(nuclearReactor, "2")
                if (valve.getState().toString().equals("Open")) {
                    jso += "{\"estado\":\"ON\",\"numeroValvula\":\"2\"},"
                } else {
                    jso += "{\"estado\":\"OFF\",\"numeroValvula\":\"2\"},"
                }

                valve = Valve.findByNuclearReactorAndIdentifier(nuclearReactor, "3")
                if (valve.getState().toString().equals("Open")) {
                    jso += "{\"estado\":\"ON\",\"numeroValvula\":\"3\"}],"
                } else {
                    jso += "{\"estado\":\"OFF\",\"numeroValvula\":\"3\"}],"
                }

                jso +="\"temperaturaDTO\":{\"valorHumedad\":\""+nuclearReactor.coolantSystem.humidityPercentage.toString()+"\",\"valorTemperatura\":\""+nuclearReactor.coolantSystem.currentTemperature.toString()+"\"}}"
            } catch (Exception e){
                jso = "{\"ERROR1\":\""+e.getMessage()+"\"}"
            }
        }

        def slurper = new JsonSlurper()
        def result = slurper.parseText(jso)
        render result as JSON
        return
    }
}
