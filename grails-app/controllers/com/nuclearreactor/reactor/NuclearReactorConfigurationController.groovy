package com.nuclearreactor.reactor

import grails.plugin.springsecurity.annotation.Secured

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class NuclearReactorConfigurationController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    @Secured(['ROLE_ADMIN', 'ROLE_ADMIN_NUCLEAR_REACTOR', 'ROLE_OPERATOR_NUCLEAR_REACTOR'])
    def index(Integer max) {
        redirect(action: 'show', id: NuclearReactor.findByCode("2XD1AS").configuration.id)
    }

    @Secured(['ROLE_ADMIN', 'ROLE_ADMIN_NUCLEAR_REACTOR', 'ROLE_OPERATOR_NUCLEAR_REACTOR'])
    def show(NuclearReactorConfiguration nuclearReactorConfigurationInstance) {
        respond nuclearReactorConfigurationInstance
    }

    @Secured(['ROLE_ADMIN', 'ROLE_ADMIN_NUCLEAR_REACTOR', 'ROLE_OPERATOR_NUCLEAR_REACTOR'])
    def edit(NuclearReactorConfiguration nuclearReactorConfigurationInstance) {
        respond nuclearReactorConfigurationInstance
    }

    @Secured(['ROLE_ADMIN', 'ROLE_ADMIN_NUCLEAR_REACTOR', 'ROLE_OPERATOR_NUCLEAR_REACTOR'])
    @Transactional
    def update(NuclearReactorConfiguration nuclearReactorConfigurationInstance) {
        if (nuclearReactorConfigurationInstance == null) {
            notFound()
            return
        }

        if (nuclearReactorConfigurationInstance.hasErrors()) {
            respond nuclearReactorConfigurationInstance.errors, view: 'edit'
            return
        }

        nuclearReactorConfigurationInstance.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'NuclearReactorConfiguration.label', default: 'NuclearReactorConfiguration'), nuclearReactorConfigurationInstance.id])
                redirect nuclearReactorConfigurationInstance
            }
            '*' { respond nuclearReactorConfigurationInstance, [status: OK] }
        }
    }
    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'nuclearReactorConfiguration.label', default: 'NuclearReactorConfiguration'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
