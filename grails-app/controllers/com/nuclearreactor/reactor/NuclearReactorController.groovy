package com.nuclearreactor.reactor

import grails.plugin.springsecurity.annotation.Secured

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class NuclearReactorController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    @Secured(['ROLE_ADMIN', 'ROLE_ADMIN_NUCLEAR_REACTOR', 'ROLE_OPERATOR_NUCLEAR_REACTOR'])
    def index(Integer max) {
        redirect(action: 'show', id: NuclearReactor.findByCode("2XD1AS").id)
    }

    @Secured(['ROLE_ADMIN', 'ROLE_ADMIN_NUCLEAR_REACTOR', 'ROLE_OPERATOR_NUCLEAR_REACTOR'])
    def show(NuclearReactor nuclearReactorInstance) {
        respond nuclearReactorInstance
    }

    @Secured(['ROLE_ADMIN', 'ROLE_ADMIN_NUCLEAR_REACTOR', 'ROLE_OPERATOR_NUCLEAR_REACTOR'])
    def edit(NuclearReactor nuclearReactorInstance) {
        respond nuclearReactorInstance
    }

    @Secured(['ROLE_ADMIN', 'ROLE_ADMIN_NUCLEAR_REACTOR', 'ROLE_OPERATOR_NUCLEAR_REACTOR'])
    @Transactional
    def update(NuclearReactor nuclearReactorInstance) {
        if (nuclearReactorInstance == null) {
            notFound()
            return
        }

        if (nuclearReactorInstance.hasErrors()) {
            respond nuclearReactorInstance.errors, view: 'edit'
            return
        }

        nuclearReactorInstance.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'NuclearReactor.label', default: 'NuclearReactor'), nuclearReactorInstance.id])
                redirect nuclearReactorInstance
            }
            '*' { respond nuclearReactorInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(NuclearReactor nuclearReactorInstance) {

        if (nuclearReactorInstance == null) {
            notFound()
            return
        }

        nuclearReactorInstance.delete flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'NuclearReactor.label', default: 'NuclearReactor'), nuclearReactorInstance.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'nuclearReactor.label', default: 'NuclearReactor'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
