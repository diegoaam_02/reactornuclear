/**
 * Created by diegoamaya on 17/08/15.
 */

var nuclearReactorWS = {}
$(function(){
    nuclearReactorWS = {
        init : function(){
            var socket = new SockJS(sockJsSocketPath);
            var client = Stomp.over(socket);
            client.connect({}, function() {
                client.subscribe("/topic/alerts/"+'2XD1AS', function(message) {
                    var nuclearReactor = JSON.parse(message.body)
                    if(nuclearReactor != null){
                        $('#nucleusTemperatureId').html(nuclearReactor.nucleusTemperature)
                        $('#waterPressure').html(nuclearReactor.coolantSystem.waterPressure)
                        if(nuclearReactor.curState.name == 'Ok'){
                            $('#trafficImage').attr("src", assetsPath+'/green.png')
                        }else if(nuclearReactor.curState.name == 'Warning'){
                            $('#trafficImage').attr("src", assetsPath+'/yellow.png')
                        }else if(nuclearReactor.curState.name == 'Bad'){
                            $('#trafficImage').attr("src", assetsPath+'/red.png')
                        }
                    }
                });
            });

        }
    }
})
$(document).ready(function(){
    nuclearReactorWS.init();
})
