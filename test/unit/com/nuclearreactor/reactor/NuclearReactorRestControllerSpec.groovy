package com.nuclearreactor.reactor

import com.nuclearreactor.reactor.rest.NuclearReactorRestController
import grails.converters.JSON
import grails.plugins.rest.client.RestBuilder
import grails.plugins.rest.client.RestResponse
import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(NuclearReactorRestController)
@Mock(NuclearReactor)

class NuclearReactorRestControllerSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "test something"() {
    }

    //***************getCurrentTemperature REST tests******************//

    def "Functional rest test for no reactor parameter"()
    {
        given:
        RestBuilder rest = new RestBuilder()

        when:
        RestResponse response
        response = rest.get("http://localhost:8080/ReactorNuclear/NuclearReactorRest/getCurrentTemperature") {
            accept JSON
        }

        then:
        assert response.status == 200
        assert response.json.success == false
        assert response.json.message  == 'You must provide the code of the reactor'
    }

    def "Functional rest test for reactor id 2XD1AS"()
    {
        given:
        RestBuilder rest = new RestBuilder()

        when:
        RestResponse response
        response = rest.get("http://localhost:8080/ReactorNuclear/NuclearReactorRest/getCurrentTemperature?code=2XD1AS&user=appUser&validationKey=43a2654bc8216af4003bba4879757639") {
            accept JSON
        }
        then:
        assert response.status == 200
        assert response.json.success == true
        //assert response.json.data.coolantSystem.currentTemperature.isNumber()
        assert response.json.data.coolantSystem.currentTemperature == 21.0
    }

    def "Functional rest test for reactor id invalid"()
    {
        given:
        RestBuilder rest = new RestBuilder()

        when:
        RestResponse response
        response = rest.get("http://localhost:8080/ReactorNuclear/NuclearReactorRest/getCurrentTemperature?code=FAKE") {
            accept JSON
        }

        then:
        assert response.status == 200
        assert response.json.success == false
        assert response.json.message  == 'Nuclear reactor was not found'
    }


    def "Test on controller for no reactor parameter"()
    {
        given:
        def jsonResponse

        when:
        controller.getCurrentTemperature()
        jsonResponse = controller.response.json

        then:
        assert jsonResponse.success== false
        assert jsonResponse.message  =='You must provide the code of the reactor'
    }



    def "Test on controller for no wrong reactor id"()
    {
        given:
        def jsonResponse

        when:

        controller.getCurrentTemperature("a2XD1AS")
        jsonResponse = controller.response.json

        then:
        assert jsonResponse.success== false
        assert jsonResponse.message  =='Nuclear reactor was not found'
    }

    //***************getReactorInformation REST tests*****************//

    def "Test on controller for reactor get information no id"()
    {
        given:
        def jsonResponse

        when:

        controller.getReactorInformation()
        jsonResponse = controller.response.json

        then:
        assert jsonResponse.success== false
        assert jsonResponse.message  =='You must provide the code of the reactor'
    }

    def "Test on controller for reactor information wrong id"()
    {
        given:
        def jsonResponse
        //def params

        when:
        params["code"] = 'invalidCode'
        controller.getReactorInformation()
        jsonResponse = controller.response.json

        then:
        assert jsonResponse.success== false
        assert jsonResponse.message  =='Nuclear reactor was not found'
    }

    //TODO emular datos de DB
    def "Test on controller for reactor information right id"()
    {
        given:
        def jsonResponse

        when:
        params["code"] = '2XD1AS'
        controller.getReactorInformation()
        jsonResponse = controller.response.json

        then:
        assert jsonResponse.success== true
    }


    def "Functional rest test for get reactor info id 2XD1AS"()
    {
        given:
        RestBuilder rest = new RestBuilder()

        when:
        RestResponse response
        response = rest.get("http://localhost:8080/ReactorNuclear/NuclearReactorRest/getReactorInformation?code=2XD1AS") {
            accept JSON
        }
        then:
        assert response.status == 200
        assert response.json.success == true
        /*{"data":{"nuclearReactor":{"code":"2XD1AS","pumpState":{"name":"Inactive","value":"OFF"},"rodsState":{"name":"Down","value":"OFF"},"lastUpdate":"2015-10-18T23:48:48Z","coolantSystem":{"humidityPercentage":0.5,"currentTemperature":25.0},"valves":[{"state":{"name":"Closed","value":"OFF"},"identifier":"3"},{"state":{"name":"Closed","value":"OFF"},"identifier":"2"},{"state":{"name":"Closed","value":"OFF"},"identifier":"1"}]}},"message":"Success","success":true}*/
    }

    def "Functional rest test for get reactor info wrong id"()
    {
        given:
        RestBuilder rest = new RestBuilder()

        when:
        RestResponse response
        response = rest.get("http://localhost:8080/ReactorNuclear/NuclearReactorRest/getReactorInformation?code=WRONGID") {
            accept JSON
        }
        then:

        assert response.json.success == false
        assert response.json.message  =='Nuclear reactor was not found'
        /*{"data":{"nuclearReactor":{"code":"2XD1AS","pumpState":{"name":"Inactive","value":"OFF"},"rodsState":{"name":"Down","value":"OFF"},"lastUpdate":"2015-10-18T23:48:48Z","coolantSystem":{"humidityPercentage":0.5,"currentTemperature":25.0},"valves":[{"state":{"name":"Closed","value":"OFF"},"identifier":"3"},{"state":{"name":"Closed","value":"OFF"},"identifier":"2"},{"state":{"name":"Closed","value":"OFF"},"identifier":"1"}]}},"message":"Success","success":true}*/
    }


    def "Functional rest test for get reactor info no id"()
    {
        given:
        RestBuilder rest = new RestBuilder()

        when:
        RestResponse response
        response = rest.get("http://localhost:8080/ReactorNuclear/NuclearReactorRest/getReactorInformation") {
            accept JSON
        }
        then:

        assert response.json.success == false
        assert response.json.message  =='You must provide the code of the reactor'
        /*{"data":{"nuclearReactor":{"code":"2XD1AS","pumpState":{"name":"Inactive","value":"OFF"},"rodsState":{"name":"Down","value":"OFF"},"lastUpdate":"2015-10-18T23:48:48Z","coolantSystem":{"humidityPercentage":0.5,"currentTemperature":25.0},"valves":[{"state":{"name":"Closed","value":"OFF"},"identifier":"3"},{"state":{"name":"Closed","value":"OFF"},"identifier":"2"},{"state":{"name":"Closed","value":"OFF"},"identifier":"1"}]}},"message":"Success","success":true}*/
    }

}
