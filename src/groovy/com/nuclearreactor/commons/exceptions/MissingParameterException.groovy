package com.nuclearreactor.commons.exceptions

/**
 * Created by diegoamaya on 20/09/15.
 */
class MissingParameterException extends Exception{

    public MissingParameterException(){
        super("Missing Parameter")
    }
    public MissingParameterException(String message){
        super(message)
    }
}