package com.nuclearreactor.commons.entities

/**
 * This class represents the structure
 *
 * @author <a href="emailto:diegoaam_02@hotmail.com">Diego Amaya</a>
 * @version 30/08/2015 - <a href="emailto:diegoaam_02@hotmail.com">Diego Amaya</a> - First Version
 * @since 30/08/2015.
 */
public class JSONResponse{

    boolean success
    String message
    def data

    public JSONResponse(){
        success = false
    }
}
