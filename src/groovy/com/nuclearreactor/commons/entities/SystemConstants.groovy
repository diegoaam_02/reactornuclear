package com.nuclearreactor.commons.entities

/**
 * Core System Constants
 *
 * @author <a href="emailto:diegoaam_02@hotmail.com">Diego Amaya</a>
 * @version 17/08/2015 - <a href="emailto:diegoaam_02@hotmail.com">Diego Amaya</a> - First Version
 * @since 17/08/2015
 */
class SystemConstants {

    /**
     ** ROLES **
     **/
    //Role for Administrator
    public final static String ROLE_ADMIN = 'ROLE_ADMIN'
    //Role for Administrator Reactor Nuclear
    public final static String ROLE_ADMIN_NUCLEAR_REACTOR = 'ROLE_ADMIN_NUCLEAR_REACTOR'
    //Role for Reactor Nuclear Operator
    public final static String ROLE_OPERATOR_NUCLEAR_REACTOR = 'ROLE_OPERATOR_NUCLEAR_REACTOR'

    /**
     ** EMAILS **
     **/
    //Admin email
    public final static String DEFAULT_ADMIN_EMAIL = 'lam_neo@hotmail.com'

    public final static String DEFAULT_ADM_NR_EMAIL = 'lampreahernan@gmail.com'

    /**
     * URLS FINAL PROJECT
     */
    //REAL: http://190.26.181.199:7001/InformaticaIMediacion/udrn15energymediador/reactorNuclear/inforeactor
    //Local: http://localhost:8080/ReactorNuclear/assets/reactorInfo.json
    //Jelastic: http://localhost:8080/ReactorNuclear/assets/reactorInfo.json
    public final static String GET_REACTOR_INFO_URL = 'http://localhost:8080/ReactorNuclear/assets/reactorInfo.json'

    //REAL: http://186.155.97.239:7001/InformaticaI/udrn15energy/reactorNuclear/infotemp
    //Local: http://localhost:8080/ReactorNuclear/assets/temperature.json
    //Jelastic: http://localhost:8080/ReactorNuclear/assets/temperature.json
    public final static String GET_TEMPERATURE_URL = 'http://190.26.181.199:7001/InformaticaIMediacion/udrn15energymediador/reactorNuclear/infotemp'

    //REAL: http://186.155.97.239:7001/InformaticaI/udrn15energy/reactorNuclear/valvula/{0}/{1}
    //Local: http://localhost:8080/ReactorNuclear/nuclearReactorRest/setValveStateTest/{0}/{1}
    //Jelastic: http://localhost:8080/ReactorNuclear/nuclearReactorRest/setValveStateTest/{0}/{1}
    public final static String SET_VALVE_STATE_URL = 'http://190.26.181.199:7001/InformaticaIMediacion/udrn15energymediador/reactorNuclear/valvula/{0}/{1}'

    //REAL: http://186.155.97.239:7001/InformaticaI/udrn15energy/reactorNuclear/bomba/{0}
    //Local: http://localhost:8080/ReactorNuclear/nuclearReactorRest/setRodsStateTest/{0}
    //Jelastic: http://localhost:8080/ReactorNuclear/nuclearReactorRest/setRodsStateTest/{0}
    public final static String SET_RODS_STATE_URL = 'http://190.26.181.199:7001/InformaticaIMediacion/udrn15energymediador/reactorNuclear/barra/{0}'
    //REAL: http://186.155.97.239:7001/InformaticaI/udrn15energy/reactorNuclear/bomba/{0}
    //Local: http://localhost:8080/ReactorNuclear/nuclearReactorRest/setRodsStateTest/{0}
    //Jelastic: http://localhost:8080/ReactorNuclear/nuclearReactorRest/setRodsStateTest/{0}
    public final static String SET_PUMP_STATE_URL = 'http://190.26.181.199:7001/InformaticaIMediacion/udrn15energymediador/reactorNuclear/bomba/{0}'
    //REAL: http://186.155.97.239:7001/InformaticaI/udrn15energy/reactorNuclear/bomba/{0}
    //Local: http://localhost:8080/ReactorNuclear/nuclearReactorRest/setRodsStateTest/{0}
    //Jelastic: http://localhost:8080/ReactorNuclear/nuclearReactorRest/setRodsStateTest/{0}
    public final static String SET_HEATER_STATE_URL = 'http://190.26.181.199:7001/InformaticaIMediacion/udrn15energymediador/reactorNuclear/termo/{0}'

    /**
     * WEB SOCKET CONSTANTS
     */

    public final static String ALERTS_TOPIC = '/topic/alerts/'

}
