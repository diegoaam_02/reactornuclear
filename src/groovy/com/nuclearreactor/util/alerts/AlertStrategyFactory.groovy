package com.nuclearreactor.util.alerts

/**
 * Created by diegoamaya on 10/09/15.
 */
interface AlertStrategyFactory {

    AlertStrategyService getStrategy(String strategyName);

}