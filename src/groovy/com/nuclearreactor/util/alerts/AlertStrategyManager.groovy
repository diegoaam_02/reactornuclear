package com.nuclearreactor.util.alerts

/**
 * Created by diegoamaya on 10/09/15.
 */
interface AlertStrategyManager {

    /**
     *
     * @param messageParam
     * @return
     */
    def sendWebSocketAlert(Object... messageParam)

    /**
     *
     * @param messageParam
     * @return
     */
    def sendRestAlert(Object... messageParam)

    /**
     * @param messageParam
     * @return
     */
    def sendEmailAlert(Object... messageParam)
}
