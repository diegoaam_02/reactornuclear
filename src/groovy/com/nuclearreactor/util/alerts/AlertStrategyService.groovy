package com.nuclearreactor.util.alerts

/**
 * Created by diegoamaya on 9/09/15.
 */
interface AlertStrategyService {
    void sendAlert(Object... params)
}