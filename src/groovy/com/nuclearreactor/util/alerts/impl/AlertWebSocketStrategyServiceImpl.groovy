package com.nuclearreactor.util.alerts.impl

import com.nuclearreactor.util.alerts.AlertStrategyService

/**
 * Created by diegoamaya on 9/09/15.
 */
class AlertWebSocketStrategyServiceImpl implements AlertStrategyService{

    def brokerMessagingTemplate

    @Override
    void sendAlert(Object... params) {
        String topic = params[0] ?: null
        String messageToTransmit = params[1] ?: null
        brokerMessagingTemplate.convertAndSend topic, messageToTransmit
    }
}
