package com.nuclearreactor.util.alerts.impl

import com.nuclearreactor.util.alerts.AlertStrategyManager
import com.nuclearreactor.util.alerts.AlertStrategyService

/**
 * Created by diegoamaya on 10/09/15.
 */
class AlertStrategyManagerImpl implements AlertStrategyManager{

    def alertStrategyFactory
    def alertStrategyService

    def sendWebSocketAlert(Object... messageParam){
        alertStrategyService = alertStrategyFactory.getStrategy('alertWebSocketStrategyService')
        ((AlertStrategyService) alertStrategyService).sendAlert(messageParam)
    }

    def sendRestAlert(Object... messageParam){
        alertStrategyService = alertStrategyFactory.getStrategy('alertRestStrategyService')
        ((AlertStrategyService) alertStrategyService).sendAlert(messageParam)
    }

    def sendEmailAlert(Object... messageParam){
        alertStrategyService = alertStrategyFactory.getStrategy('alertEmailStrategyService')
        ((AlertStrategyService) alertStrategyService).sendAlert(messageParam)
    }
}
