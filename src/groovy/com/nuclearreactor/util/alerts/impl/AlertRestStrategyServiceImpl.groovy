package com.nuclearreactor.util.alerts.impl

import com.nuclearreactor.util.alerts.AlertStrategyService
import grails.plugins.rest.client.RestBuilder
import org.springframework.web.client.ResourceAccessException

/**
 * Created by diegoamaya on 20/09/15.
 */
class AlertRestStrategyServiceImpl implements AlertStrategyService{

    RestBuilder restBuilder

    @Override
        void sendAlert(Object... params) {
            String url = params[0] ?: null
            String httpMethod = params[1] ?: null
            print url
            print httpMethod
            def result
            try{
                if(httpMethod.equals("POST")){
                    result = restBuilder.post(url) {
                        accept "application/json"
                    }
                }else{
                    result = restBuilder.get(url) {
                        accept "application/json"
                    }
                }


            }catch (ResourceAccessException resourceAccessException){

            }catch(any){

            }

            result
    }
}
