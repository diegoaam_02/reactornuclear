package com.nuclearreactor.util.alerts.impl

import com.nuclearreactor.alerts.Alert
import com.nuclearreactor.alerts.AlertType
import com.nuclearreactor.util.alerts.AlertStrategyService
import grails.plugin.mail.MailService

/**
 * Created by diegoamaya on 9/09/15.
 */
class AlertEmailStrategyServiceImpl implements AlertStrategyService{

    MailService mailService

    @Override
    void sendAlert(Object... params) {

        String email = params[0] ?: null
        Alert alert = params[1] ?: null

        if(alert.type.equals(AlertType.Warning)){
            try{

                mailService.sendMail {
                    to email
                    from "nuclearreactorservidor@gmail.com"
                    subject "Nueva Alerta En el Reactor"
                    html "<p> Administrador. Se ha generado una alerta en el Reactor." +
                            " Por favor revise su estado</p>" +
                            "<br/><br/><p> Este es un Correo Automatico. Por Favor No responder. </p>"
                }
            }catch (Exception err){
                err.printStackTrace()
                print("ERROR")
            }
        }

    }

}
