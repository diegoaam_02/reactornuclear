package com.nuclearreactor.audits

import org.aspectj.lang.JoinPoint
import org.aspectj.lang.annotation.Aspect
import org.aspectj.lang.annotation.Before

/**
 * Created by diegoamaya on 21/09/15.
 */
@Aspect
class AuditAspect {

    AuditsService auditsService

    @Before("execution(* com.nuclearreactor.reactor.impl.NuclearReactorHistoryServiceImpl.updateAllNuclearReactorsFromRemote(..))")
    public void logBefore(JoinPoint joinPoint){
        String logMessage = String.format("Beginning of each method: %s.%s(%s)",
                joinPoint.getTarget().getClass().getName(),
                joinPoint.getSignature().getName(),
                Arrays.toString(joinPoint.getArgs()));
        print(logMessage)
        //ogger.info(logMessage);
    }
}
