package com.nuclearreactor.alerts.exceptions

/**
 * Created by diegoamaya on 20/09/15.
 */
class AlertNotSendException extends Exception{

    public AlertNotSendException(Throwable cause) {
        super("The alert has not been send",cause);
    }

    public AlertNotSendException(){
        super("The alert has not been send")
    }
    public AlertNotSendException(String message){
        super(message)
    }

}