package com.nuclearreactor.alerts.exceptions

/**
 * Created by diegoamaya on 20/09/15.
 */
class AlertConfigurationNotSetException extends Exception{

    public AlertConfigurationNotSetException(){
        super("Alerts configurations has not been set for the nuclear reactor")
    }
    public AlertConfigurationNotSetException(String message){
        super(message)
    }
}