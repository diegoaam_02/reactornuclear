package com.nuclearreactor.reactor.exceptions

/**
 * Created by Jorge on 23/10/2015.
 */
class SetHeaterStateURLNotSetException extends Exception{

    public SetHeaterStateURLNotSetException(){
        super("The URL to set the Heater state has not been configured yet")
    }
    public SetHeaterStateURLNotSetException(String message){
        super(message)
    }
}
