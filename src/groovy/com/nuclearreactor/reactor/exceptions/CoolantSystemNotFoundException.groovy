package com.nuclearreactor.reactor.exceptions

/**
 * Created by diegoamaya on 20/09/15.
 */
class CoolantSystemNotFoundException extends Exception{

    public CoolantSystemNotFoundException(){
        super("The coolant system has not been set for this nucler reactor")
    }
    public CoolantSystemNotFoundException(String message){
        super(message)
    }
}
