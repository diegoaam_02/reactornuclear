package com.nuclearreactor.reactor.exceptions

/**
 * Created by diegoamaya on 31/08/15.
 */
class ValveNotFoundException extends Exception{

    public ValveNotFoundException(){
        super("The valve was not found")
    }
    public ValveNotFoundException(String message){
        super(message)
    }
}
