package com.nuclearreactor.reactor.exceptions

/**
 * Created by diegoamaya on 28/08/15.
 */
class ValveStateRequestTimeOutException extends Exception{

    public ValveStateRequestTimeOutException(){
        super("Time Out")
    }
    public ValveStateRequestTimeOutException(String message){
        super(message)
    }
}
