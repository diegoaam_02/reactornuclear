package com.nuclearreactor.reactor.exceptions

/**
 * Created by diegoamaya on 28/08/15.
 */
class NuclearRequestTimeOutException extends Exception{

    public NuclearRequestTimeOutException(){
        super("Time Out")
    }
    public NuclearRequestTimeOutException(String message){
        super(message)
    }

    public NuclearRequestTimeOutException(Throwable cause) {
        super("Time Out",cause);
    }
}
