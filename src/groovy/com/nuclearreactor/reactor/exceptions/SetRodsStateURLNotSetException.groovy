package com.nuclearreactor.reactor.exceptions

/**
 * Created by diegoamaya on 31/08/15.
 */
class SetRodsStateURLNotSetException extends Exception{

    public SetRodsStateURLNotSetException(){
        super("The URL to set the rods state has not been configured yet")
    }
    public SetRodsStateURLNotSetException(String message){
        super(message)
    }
}
