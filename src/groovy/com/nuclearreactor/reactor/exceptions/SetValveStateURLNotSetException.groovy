package com.nuclearreactor.reactor.exceptions

/**
 * Created by diegoamaya on 31/08/15.
 */
class SetValveStateURLNotSetException extends Exception{

    public SetValveStateURLNotSetException(){
        super("The URL to set the valve state has not been configured yet")
    }
    public SetValveStateURLNotSetException(String message){
        super(message)
    }
}
