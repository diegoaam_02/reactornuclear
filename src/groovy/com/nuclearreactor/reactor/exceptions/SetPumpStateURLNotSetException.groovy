package com.nuclearreactor.reactor.exceptions

/**
 * Created by Jorge on 21/10/2015.
 */
class SetPumpStateURLNotSetException extends  Exception{

    public SetPumpStateURLNotSetException(){
        super("The URL to set the Pump state has not been configured yet")
    }
    public SetPumpStateURLNotSetException(String message){
        super(message)
    }
}
