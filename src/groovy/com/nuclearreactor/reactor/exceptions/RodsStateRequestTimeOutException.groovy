package com.nuclearreactor.reactor.exceptions

/**
 * Created by diegoamaya on 28/08/15.
 */
class RodsStateRequestTimeOutException extends Exception{

    public RodsStateRequestTimeOutException(){
        super("Time Out")
    }
    public RodsStateRequestTimeOutException(String message){
        super(message)
    }
}
