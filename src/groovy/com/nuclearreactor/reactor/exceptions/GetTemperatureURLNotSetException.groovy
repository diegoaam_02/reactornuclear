package com.nuclearreactor.reactor.exceptions

/**
 * Created by diegoamaya on 28/08/15.
 */
class GetTemperatureURLNotSetException extends Exception{

    public GetTemperatureURLNotSetException(){
        super("URL to get current temperature has not been set")
    }
    public GetTemperatureURLNotSetException(String message){
        super(message)
    }
}
