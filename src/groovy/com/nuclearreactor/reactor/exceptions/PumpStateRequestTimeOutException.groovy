package com.nuclearreactor.reactor.exceptions

/**
 * Created by Jorge on 21/10/2015.
 */
class PumpStateRequestTimeOutException extends Exception{

    public PumpStateRequestTimeOutException(){
        super("Time Out")
    }
    public PumpStateRequestTimeOutException(String message){
        super(message)
    }
}

