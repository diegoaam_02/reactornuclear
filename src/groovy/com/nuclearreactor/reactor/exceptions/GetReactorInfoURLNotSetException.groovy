package com.nuclearreactor.reactor.exceptions

/**
 * Created by diegoamaya on 28/08/15.
 */
class GetReactorInfoURLNotSetException extends Exception{

    public GetReactorInfoURLNotSetException(){
        super("URL to get the reactor information has not been set")
    }
    public GetReactorInfoURLNotSetException(String message){
        super(message)
    }

    public GetReactorInfoURLNotSetException(Throwable cause) {
        super("URL to get the reactor information has not been set",cause);
    }
}
