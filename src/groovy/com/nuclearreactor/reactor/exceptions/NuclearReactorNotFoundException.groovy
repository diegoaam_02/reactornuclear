package com.nuclearreactor.reactor.exceptions

/**
 * Created by diegoamaya on 28/08/15.
 */
class NuclearReactorNotFoundException extends Exception{

    public NuclearReactorNotFoundException(){
        super("Nuclear Reactor not found")
    }
    public NuclearReactorNotFoundException(String message){
        super(message)
    }
}
