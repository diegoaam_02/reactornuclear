package com.nuclearreactor.reactor.exceptions

/**
 * Created by Jorge on 23/10/2015.
 */
class HeaterStateRequestTimeOutException extends Exception{
    public HeaterStateRequestTimeOutException(){
        super("Time Out")
    }
    public HeaterStateRequestTimeOutException(String message){
        super(message)
    }
}
